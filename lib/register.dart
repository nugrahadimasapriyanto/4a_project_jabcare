import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:login_flutter/home.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordValidasiController =
      TextEditingController();

  bool errorValidator = false;
  Future signUp() async {
    try {
      final credential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );
      await FirebaseAuth.instance.signOut();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("SignUp Page"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(50.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10.0,
            ),
            Text(
              "Email",
            ),
            SizedBox(
              height: 15.0,
            ),
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Tuliskan Email",
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              "Password",
            ),
            SizedBox(
              height: 15.0,
            ),
            TextField(
              controller: passwordController,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Tuliskan Password",
                errorText: errorValidator ? "Password tidak sesuai" : null,
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            Text(
              "Validasi Password",
            ),
            SizedBox(
              height: 15.0,
            ),
            TextField(
              controller: passwordValidasiController,
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Tuliskan Password",
                errorText: errorValidator ? "Password tidak sesuai" : null,
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
            SizedBox(
              width: double.infinity,
              height: 45.0,
              child: ElevatedButton(
                onPressed: () {
                  setState(() {
                    passwordController.text == passwordValidasiController.text
                        ? errorValidator = false
                        : errorValidator = true;
                  });
                  if (errorValidator) {
                    print("Error");
                  } else {
                    signUp();
                    Navigator.pop(context);
                  }
                },
                child: Text("Daftar"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
