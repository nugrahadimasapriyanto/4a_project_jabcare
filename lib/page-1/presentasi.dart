import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/halamanutama.dart';
import 'package:login_flutter/utils.dart';

class presentasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // datainformasiE4e (22:1003)
        padding: EdgeInsets.fromLTRB(0 * fem, 10.76 * fem, 0 * fem, 14 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogrouprepqikW (jj6hY832Cn8QGfXdcREPQ)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 86 * fem, 11 * fem),
              width: 369 * fem,
              height: 124.24 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // jabcare1326 (607:1170)
                    left: 0 * fem,
                    top: 0.2440490723 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 289 * fem,
                        height: 124 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-43Y.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1x94 (607:1171)
                    left: 226 * fem,
                    top: 8.2440490723 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-s6E.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // layer26g6 (607:1154)
                    left: 5 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 40 * fem,
                        height: 42.89 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => halamanutama()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/layer-2-DNv.png',
                            width: 40 * fem,
                            height: 42.89 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // datapresentasim6W (607:1180)
                    left: 25 * fem,
                    top: 91.2440490723 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 137 * fem,
                        height: 33 * fem,
                        child: Text(
                          'Data Presentasi',
                          textAlign: TextAlign.center,
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 20 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupnmd8q6N (jj6qHQ8QszcafaNVDNMD8)
              margin:
                  EdgeInsets.fromLTRB(35 * fem, 0 * fem, 37 * fem, 22 * fem),
              width: double.infinity,
              height: 47 * fem,
              child: Stack(
                children: [
                  // Positioned(
                  //   // searchbarA8e (166:873)
                  //   left: 0 * fem,
                  //   top: 0 * fem,
                  //   child: Container(
                  //     width: 355 * fem,
                  //     height: 46 * fem,
                  //     decoration: BoxDecoration(
                  //       boxShadow: [
                  //         BoxShadow(
                  //           color: Color(0x3f000000),
                  //           offset: Offset(0 * fem, 4 * fem),
                  //           blurRadius: 2 * fem,
                  //         ),
                  //       ],
                  //     ),
                  //     child: Container(
                  //       // contentSrr (I166:873;2:44)
                  //       padding: EdgeInsets.fromLTRB(
                  //           8 * fem, 6 * fem, 8 * fem, 1 * fem),
                  //       width: double.infinity,
                  //       height: 36 * fem,
                  //       decoration: BoxDecoration(
                  //         color: Color(0xedfafafa),
                  //         borderRadius: BorderRadius.circular(10 * fem),
                  //       ),
                  //       child: Row(
                  //         crossAxisAlignment: CrossAxisAlignment.center,
                  //         children: [
                  //           Opacity(
                  //             // imicandroidUoY (I166:873;2:45)
                  //             opacity: 0,
                  //             child: Container(
                  //               margin: EdgeInsets.fromLTRB(
                  //                   0 * fem, 0 * fem, 118.5 * fem, 5 * fem),
                  //               width: 24 * fem,
                  //               height: 24 * fem,
                  //               child: Image.asset(
                  //                 'assets/page-1/images/i-mic-android-LGv.png',
                  //                 width: 24 * fem,
                  //                 height: 24 * fem,
                  //               ),
                  //             ),
                  //           ),
                  //           Container(
                  //             // contARU (I166:873;2:46)
                  //             margin: EdgeInsets.fromLTRB(
                  //                 0 * fem, 1 * fem, 125.5 * fem, 0 * fem),
                  //             height: 28 * fem,
                  //             child: Text(
                  //               'Search',
                  //               style: SafeGoogleFont(
                  //                 'SF Pro Text',
                  //                 fontSize: 17 * ffem,
                  //                 fontWeight: FontWeight.w400,
                  //                 height: 1.2941176471 * ffem / fem,
                  //                 letterSpacing: -0.4079999924 * fem,
                  //                 color: Color(0x993c3c43),
                  //               ),
                  //             ),
                  //           ),
                  //           Container(
                  //             // miceLe (I166:873;2:49)
                  //             margin: EdgeInsets.fromLTRB(
                  //                 0 * fem, 0 * fem, 0 * fem, 5 * fem),
                  //             width: 24 * fem,
                  //             height: 24 * fem,
                  //             child: Image.asset(
                  //               'assets/page-1/images/mic.png',
                  //               width: 24 * fem,
                  //               height: 24 * fem,
                  //             ),
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  // Positioned(
                  //   // searchbarMkr (606:1056)
                  //   left: 1 * fem,
                  //   top: 1 * fem,
                  //   child: Container(
                  //     width: 355 * fem,
                  //     height: 46 * fem,
                  //     decoration: BoxDecoration(
                  //       boxShadow: [
                  //         BoxShadow(
                  //           color: Color(0x3f000000),
                  //           offset: Offset(0 * fem, 4 * fem),
                  //           blurRadius: 2 * fem,
                  //         ),
                  //       ],
                  //     ),
                  //     child: Align(
                  //       // contentTJ6 (I606:1056;2:44)
                  //       alignment: Alignment.topCenter,
                  //       child: SizedBox(
                  //         width: double.infinity,
                  //         height: 36 * fem,
                  //         child: Container(
                  //           decoration: BoxDecoration(
                  //             borderRadius: BorderRadius.circular(10 * fem),
                  //             color: Color(0xedfafafa),
                  //           ),
                  //           child: TextField(
                  //             decoration: InputDecoration(
                  //               border: InputBorder.none,
                  //               focusedBorder: InputBorder.none,
                  //               enabledBorder: InputBorder.none,
                  //               errorBorder: InputBorder.none,
                  //               disabledBorder: InputBorder.none,
                  //               contentPadding: EdgeInsets.fromLTRB(
                  //                   8 * fem, 6 * fem, 8 * fem, 1 * fem),
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
            Container(
              // jawabarat2022dbt (125:860)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 1 * fem, 0 * fem),
              child: Text(
                'Jawa Barat 2022',
                style: SafeGoogleFont(
                  'Catamaran',
                  fontSize: 20 * ffem,
                  fontWeight: FontWeight.w700,
                  height: 1.64 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
            Container(
              // rectangle39kAi (107:828)
              margin:
                  EdgeInsets.fromLTRB(51 * fem, 0 * fem, 52 * fem, 29 * fem),
              width: double.infinity,
              height: 250 * fem,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10 * fem),
                border: Border.all(color: Color(0xffcacaca)),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(
                    'assets/page-1/images/rectangle-39-bg.png',
                  ),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
            ),
            Container(
              // grafikRXk (125:861)
              margin:
                  EdgeInsets.fromLTRB(0 * fem, 0 * fem, 263 * fem, 21 * fem),
              child: Text(
                'Grafik',
                style: SafeGoogleFont(
                  'Catamaran',
                  fontSize: 20 * ffem,
                  fontWeight: FontWeight.w700,
                  height: 1.64 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
            Container(
              // image26wkz (125:834)
              margin: EdgeInsets.fromLTRB(1 * fem, 0 * fem, 0 * fem, 26 * fem),
              width: 353 * fem,
              height: 236 * fem,
              child: Image.asset(
                'assets/page-1/images/image-26.png',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              // rectangle40TjL (125:835)
              margin:
                  EdgeInsets.fromLTRB(0 * fem, 0 * fem, 10 * fem, 117 * fem),
              width: 280 * fem,
              height: 158 * fem,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(5 * fem),
                child: Image.asset(
                  'assets/page-1/images/rectangle-40.png',
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Container(
              // ketentuanpenggunaankebijakanpr (125:836)
              constraints: BoxConstraints(
                maxWidth: 230 * fem,
              ),
              child: Text(
                'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Gilda Display',
                  fontSize: 12 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.1775 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
