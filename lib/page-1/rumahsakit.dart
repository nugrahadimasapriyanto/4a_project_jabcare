import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/fasilitas.dart';
import 'package:login_flutter/utils.dart';

class rumahsakit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // n2S (107:723)
        padding: EdgeInsets.fromLTRB(0 * fem, 8.75 * fem, 0 * fem, 14 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupqp9tgdc (jjAcvb9ixKkBMHDu3Qp9t)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 72 * fem, 11 * fem),
              width: 383 * fem,
              height: 126.25 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // jabcare1bka (107:726)
                    left: 0 * fem,
                    top: 2.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 289 * fem,
                        height: 124 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-742.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1v2A (107:727)
                    left: 240 * fem,
                    top: 22.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-kGW.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // layer2qev (170:938)
                    left: 5 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 40 * fem,
                        height: 43.75 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => fasilitas()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/layer-2-ogr.png',
                            width: 40 * fem,
                            height: 43.75 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            // Container(
            //   // searchbarL5t (166:949)
            //   margin:
            //       EdgeInsets.fromLTRB(35 * fem, 0 * fem, 38 * fem, 24 * fem),
            //   width: double.infinity,
            //   height: 46 * fem,
            //   decoration: BoxDecoration(
            //     boxShadow: [
            //       BoxShadow(
            //         color: Color(0x3f000000),
            //         offset: Offset(0 * fem, 4 * fem),
            //         blurRadius: 2 * fem,
            //       ),
            //     ],
            //   ),
            //   child: Align(
            //     // contentq2e (I166:949;2:44)
            //     alignment: Alignment.topCenter,
            //     child: SizedBox(
            //       width: double.infinity,
            //       height: 36 * fem,
            //       child: Container(
            //         decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(10 * fem),
            //           color: Color(0xedfafafa),
            //         ),
            //         child: TextField(
            //           decoration: InputDecoration(
            //             border: InputBorder.none,
            //             focusedBorder: InputBorder.none,
            //             enabledBorder: InputBorder.none,
            //             errorBorder: InputBorder.none,
            //             disabledBorder: InputBorder.none,
            //             contentPadding: EdgeInsets.fromLTRB(
            //                 8 * fem, 6 * fem, 8 * fem, 1 * fem),
            //           ),
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            Container(
              // image27EqU (166:946)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 13 * fem),
              width: 356 * fem,
              height: 515 * fem,
              child: Image.asset(
                'assets/page-1/images/image-27.png',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              // autogroupdylnxmU (jjAjkj6zamiqCnybfDYLn)
              margin:
                  EdgeInsets.fromLTRB(51 * fem, 0 * fem, 52 * fem, 14 * fem),
              padding:
                  EdgeInsets.fromLTRB(19 * fem, 12 * fem, 18 * fem, 24 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xffcacaca)),
                color: Color(0xffeaeaea),
                borderRadius: BorderRadius.circular(10 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Center(
                // image22qaN (107:749)
                child: SizedBox(
                  width: 288 * fem,
                  height: 214 * fem,
                  child: TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.zero,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10 * fem),
                      child: Image.asset(
                        'assets/page-1/images/image-22.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              // ketentuanpenggunaankebijakanpr (107:751)
              constraints: BoxConstraints(
                maxWidth: 230 * fem,
              ),
              child: Text(
                'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Gilda Display',
                  fontSize: 12 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.1775 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
