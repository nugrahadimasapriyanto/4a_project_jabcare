import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:login_flutter/utils.dart';
import 'halamanutama.dart';

class chat extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // chatgTG (12:6)
        padding: EdgeInsets.fromLTRB(0 * fem, 3 * fem, 0 * fem, 0 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              // autogroup4ugiNaz (jiz6j7toBEhUpA2is4uGi)
              width: 388 * fem,
              height: 132 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // jabcare15kJ (12:7)
                    left: 0 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 289 * fem,
                        height: 124 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-C9L.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // backarrowy4z (31:2078)
                    left: 25 * fem,
                    top: 14 * fem,
                    child: TextButton(
                      onPressed: () {},
                      style: TextButton.styleFrom(
                        padding: EdgeInsets.zero,
                      ),
                      child: Container(
                        width: 24 * fem,
                        height: 24 * fem,
                      ),
                    ),
                  ),
                  Positioned(
                    // layer2fiW (31:2079)
                    left: 5 * fem,
                    top: 5.0086364746 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 40 * fem,
                        height: 40.04 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => halamanutama()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/layer-2.png',
                            width: 40 * fem,
                            height: 40.04 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // chatadmin6op (64:611)
                    left: 25 * fem,
                    top: 99 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 103 * fem,
                        height: 33 * fem,
                        child: Text(
                          'Chat Admin',
                          textAlign: TextAlign.center,
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 20 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1xLE (84:602)
                    left: 272 * fem,
                    top: 20 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-4oY.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogrouprlcvG62 (jizN41h1cSJen64ayrLcv)
              width: double.infinity,
              height: 792 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // download1PRY (12:22)
                    left: 0 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 428 * fem,
                        height: 787 * fem,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10 * fem),
                          child: Image.asset(
                            'assets/page-1/images/download-1.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // iosalphbetickeyboarditalianUC6 (12:310)
                    left: 0 * fem,
                    top: 457 * fem,
                    child: Container(
                      width: 428 * fem,
                      height: 335 * fem,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            // componentsuggestionbarx7G (I12:310;352:400)
                            padding: EdgeInsets.fromLTRB(
                                1 * fem, 10 * fem, 1 * fem, 0 * fem),
                            width: double.infinity,
                            height: 44 * fem,
                            decoration: BoxDecoration(
                              color: Color(0xc1ccced3),
                            ),
                            child: ClipRect(
                              child: BackdropFilter(
                                filter: ImageFilter.blur(
                                  sigmaX: 10 * fem,
                                  sigmaY: 10 * fem,
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Container(
                                      // componentsuggestionitemyHG (I12:310;352:400;352:3040)
                                      margin: EdgeInsets.fromLTRB(
                                          0 * fem, 0 * fem, 2 * fem, 0 * fem),
                                      width: 139.33 * fem,
                                      height: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            4.5999999046 * fem),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'suggerire',
                                          textAlign: TextAlign.center,
                                          style: SafeGoogleFont(
                                            'SF Pro Text',
                                            fontSize: 17 * ffem,
                                            fontWeight: FontWeight.w400,
                                            height: 1.1764705882 * ffem / fem,
                                            color: Color(0xff3a3b3d),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      // componentsuggestionitemR9G (I12:310;352:400;352:3042)
                                      margin: EdgeInsets.fromLTRB(
                                          0 * fem, 0 * fem, 2 * fem, 0 * fem),
                                      width: 139.33 * fem,
                                      height: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            4.5999999046 * fem),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'suggerire',
                                          textAlign: TextAlign.center,
                                          style: SafeGoogleFont(
                                            'SF Pro Text',
                                            fontSize: 17 * ffem,
                                            fontWeight: FontWeight.w400,
                                            height: 1.1764705882 * ffem / fem,
                                            color: Color(0xff3a3b3d),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      // componentsuggestionitemf3c (I12:310;352:400;352:3044)
                                      width: 139.33 * fem,
                                      height: double.infinity,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            4.5999999046 * fem),
                                      ),
                                      child: Center(
                                        child: Text(
                                          'suggerire',
                                          textAlign: TextAlign.center,
                                          style: SafeGoogleFont(
                                            'SF Pro Text',
                                            fontSize: 17 * ffem,
                                            fontWeight: FontWeight.w400,
                                            height: 1.1764705882 * ffem / fem,
                                            color: Color(0xff3a3b3d),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            // keyslayoutalphabeticitaliandef (I12:310;352:401)
                            padding: EdgeInsets.fromLTRB(
                                3 * fem, 8 * fem, 3 * fem, 8 * fem),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Color(0xc1ccced3),
                            ),
                            child: ClipRect(
                              child: BackdropFilter(
                                filter: ImageFilter.blur(
                                  sigmaX: 10 * fem,
                                  sigmaY: 10 * fem,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      // rowalphabeticoZ8 (I12:310;352:401;352:2405)
                                      width: double.infinity,
                                      height: 42 * fem,
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            // componentkey8LW (I12:310;352:401;352:2406)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'Q',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyZgi (I12:310;352:401;352:2407)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'W',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyRiv (I12:310;352:401;352:2408)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'E',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyger (I12:310;352:401;352:2409)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'R',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyMW6 (I12:310;352:401;352:2410)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'T',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyDoC (I12:310;352:401;352:2411)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'Y',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeytuL (I12:310;352:401;352:2412)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'U',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyMnv (I12:310;352:401;352:2413)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'I',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyoQ2 (I12:310;352:401;352:2414)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'O',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyFWv (I12:310;352:401;352:2415)
                                            width: 37.7 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'P',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 12 * fem,
                                    ),
                                    Container(
                                      // rowalphabetici9c (I12:310;352:401;352:2416)
                                      margin: EdgeInsets.fromLTRB(
                                          18 * fem, 0 * fem, 18 * fem, 0 * fem),
                                      width: double.infinity,
                                      height: 42 * fem,
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            // componentkeyckn (I12:310;352:401;352:2417)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'A',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyfj4 (I12:310;352:401;352:2418)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'S',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyXWN (I12:310;352:401;352:2419)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'D',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyPoU (I12:310;352:401;352:2420)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'F',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyFan (I12:310;352:401;352:2421)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'G',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyVV8 (I12:310;352:401;352:2422)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'H',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyA5U (I12:310;352:401;352:2423)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'J',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeypQv (I12:310;352:401;352:2424)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 5 * fem, 0 * fem),
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'K',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkey4aA (I12:310;352:401;352:2425)
                                            width: 38.44 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'L',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 22 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height:
                                                      1.2727272727 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 12 * fem,
                                    ),
                                    Container(
                                      // rowshiftalphabeticdeleteTMQ (I12:310;352:401;352:2426)
                                      width: double.infinity,
                                      height: 42 * fem,
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            // componentkeyzML (I12:310;352:401;352:2427)
                                            width: 42 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                '􀆞',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Text',
                                                  fontSize: 16 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3125 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 14 * fem,
                                          ),
                                          Container(
                                            // rowalphabetic2op (I12:310;352:401;352:2428)
                                            height: double.infinity,
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                Container(
                                                  // componentkeyyj4 (I12:310;352:401;352:2429)
                                                  width: 40 * fem,
                                                  height: double.infinity,
                                                  decoration: BoxDecoration(
                                                    color: Color(0xffffffff),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.5999999046 * fem),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color:
                                                            Color(0xff898a8d),
                                                        offset: Offset(
                                                            0 * fem, 1 * fem),
                                                        blurRadius: 0 * fem,
                                                      ),
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'Z',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: SafeGoogleFont(
                                                        'SF Pro Display',
                                                        fontSize: 22 * ffem,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.2727272727 *
                                                            ffem /
                                                            fem,
                                                        color:
                                                            Color(0xff000000),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5 * fem,
                                                ),
                                                Container(
                                                  // componentkey3D8 (I12:310;352:401;352:2430)
                                                  width: 40 * fem,
                                                  height: double.infinity,
                                                  decoration: BoxDecoration(
                                                    color: Color(0xffffffff),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.5999999046 * fem),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color:
                                                            Color(0xff898a8d),
                                                        offset: Offset(
                                                            0 * fem, 1 * fem),
                                                        blurRadius: 0 * fem,
                                                      ),
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'X',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: SafeGoogleFont(
                                                        'SF Pro Display',
                                                        fontSize: 22 * ffem,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.2727272727 *
                                                            ffem /
                                                            fem,
                                                        color:
                                                            Color(0xff000000),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5 * fem,
                                                ),
                                                Container(
                                                  // componentkey6x6 (I12:310;352:401;352:2431)
                                                  width: 40 * fem,
                                                  height: double.infinity,
                                                  decoration: BoxDecoration(
                                                    color: Color(0xffffffff),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.5999999046 * fem),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color:
                                                            Color(0xff898a8d),
                                                        offset: Offset(
                                                            0 * fem, 1 * fem),
                                                        blurRadius: 0 * fem,
                                                      ),
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'C',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: SafeGoogleFont(
                                                        'SF Pro Display',
                                                        fontSize: 22 * ffem,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.2727272727 *
                                                            ffem /
                                                            fem,
                                                        color:
                                                            Color(0xff000000),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5 * fem,
                                                ),
                                                Container(
                                                  // componentkeyN8v (I12:310;352:401;352:2432)
                                                  width: 40 * fem,
                                                  height: double.infinity,
                                                  decoration: BoxDecoration(
                                                    color: Color(0xffffffff),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.5999999046 * fem),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color:
                                                            Color(0xff898a8d),
                                                        offset: Offset(
                                                            0 * fem, 1 * fem),
                                                        blurRadius: 0 * fem,
                                                      ),
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'V',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: SafeGoogleFont(
                                                        'SF Pro Display',
                                                        fontSize: 22 * ffem,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.2727272727 *
                                                            ffem /
                                                            fem,
                                                        color:
                                                            Color(0xff000000),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5 * fem,
                                                ),
                                                Container(
                                                  // componentkey2DU (I12:310;352:401;352:2433)
                                                  width: 40 * fem,
                                                  height: double.infinity,
                                                  decoration: BoxDecoration(
                                                    color: Color(0xffffffff),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.5999999046 * fem),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color:
                                                            Color(0xff898a8d),
                                                        offset: Offset(
                                                            0 * fem, 1 * fem),
                                                        blurRadius: 0 * fem,
                                                      ),
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'B',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: SafeGoogleFont(
                                                        'SF Pro Display',
                                                        fontSize: 22 * ffem,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.2727272727 *
                                                            ffem /
                                                            fem,
                                                        color:
                                                            Color(0xff000000),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5 * fem,
                                                ),
                                                Container(
                                                  // componentkeyGdc (I12:310;352:401;352:2434)
                                                  width: 40 * fem,
                                                  height: double.infinity,
                                                  decoration: BoxDecoration(
                                                    color: Color(0xffffffff),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.5999999046 * fem),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color:
                                                            Color(0xff898a8d),
                                                        offset: Offset(
                                                            0 * fem, 1 * fem),
                                                        blurRadius: 0 * fem,
                                                      ),
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'N',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: SafeGoogleFont(
                                                        'SF Pro Display',
                                                        fontSize: 22 * ffem,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.2727272727 *
                                                            ffem /
                                                            fem,
                                                        color:
                                                            Color(0xff000000),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5 * fem,
                                                ),
                                                Container(
                                                  // componentkey8fp (I12:310;352:401;352:2435)
                                                  width: 40 * fem,
                                                  height: double.infinity,
                                                  decoration: BoxDecoration(
                                                    color: Color(0xffffffff),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            4.5999999046 * fem),
                                                    boxShadow: [
                                                      BoxShadow(
                                                        color:
                                                            Color(0xff898a8d),
                                                        offset: Offset(
                                                            0 * fem, 1 * fem),
                                                        blurRadius: 0 * fem,
                                                      ),
                                                    ],
                                                  ),
                                                  child: Center(
                                                    child: Text(
                                                      'M',
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: SafeGoogleFont(
                                                        'SF Pro Display',
                                                        fontSize: 22 * ffem,
                                                        fontWeight:
                                                            FontWeight.w400,
                                                        height: 1.2727272727 *
                                                            ffem /
                                                            fem,
                                                        color:
                                                            Color(0xff000000),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            width: 14 * fem,
                                          ),
                                          Container(
                                            // componentkeyPre (I12:310;352:401;352:2436)
                                            width: 42 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffadb3bc),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                '􀆛',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Text',
                                                  fontSize: 16 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3125 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: 12 * fem,
                                    ),
                                    Container(
                                      // rownumbersspacegof3U (I12:310;352:401;352:2437)
                                      width: double.infinity,
                                      height: 42 * fem,
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            // componentkeybSv (I12:310;352:401;352:2438)
                                            width: 87 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffadb3bc),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                '123',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Text',
                                                  fontSize: 16 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3125 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 6 * fem,
                                          ),
                                          Container(
                                            // componentkeySTY (I12:310;352:401;352:2439)
                                            width: 235 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffffffff),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'spazio',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Text',
                                                  fontSize: 16 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3125 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            width: 6 * fem,
                                          ),
                                          Container(
                                            // componentkeygcn (I12:310;352:401;352:2440)
                                            width: 88 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              color: Color(0xffadb3bc),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Color(0xff898a8d),
                                                  offset:
                                                      Offset(0 * fem, 1 * fem),
                                                  blurRadius: 0 * fem,
                                                ),
                                              ],
                                            ),
                                            child: Center(
                                              child: Text(
                                                'partire',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Text',
                                                  fontSize: 16 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.3125 * ffem / fem,
                                                  color: Color(0xff000000),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            // componenthomeindicatorsectionM (I12:310;352:402)
                            padding: EdgeInsets.fromLTRB(
                                20 * fem, 9 * fem, 20 * fem, 9 * fem),
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Color(0xc1ccced3),
                            ),
                            child: ClipRect(
                              child: BackdropFilter(
                                filter: ImageFilter.blur(
                                  sigmaX: 10 * fem,
                                  sigmaY: 10 * fem,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      // keys1Ya (I12:310;352:402;352:3015)
                                      margin: EdgeInsets.fromLTRB(
                                          0 * fem, 0 * fem, 0 * fem, 1 * fem),
                                      width: double.infinity,
                                      height: 47 * fem,
                                      child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            // componentkeywBL (I12:310;352:402;352:3016)
                                            margin: EdgeInsets.fromLTRB(0 * fem,
                                                0 * fem, 294 * fem, 0 * fem),
                                            width: 47 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                            ),
                                            child: Center(
                                              child: Text(
                                                '􀎸',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 26 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.2575 * ffem / fem,
                                                  color: Color(0xff50555c),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Container(
                                            // componentkeyzvJ (I12:310;352:402;352:3017)
                                            width: 47 * fem,
                                            height: double.infinity,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      4.5999999046 * fem),
                                            ),
                                            child: Center(
                                              child: Text(
                                                '􀊰',
                                                textAlign: TextAlign.center,
                                                style: SafeGoogleFont(
                                                  'SF Pro Display',
                                                  fontSize: 26 * ffem,
                                                  fontWeight: FontWeight.w400,
                                                  height: 1.2575 * ffem / fem,
                                                  color: Color(0xff50555c),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      // indicatorsDQ (I12:310;352:402;352:3018;352:3059)
                                      margin: EdgeInsets.fromLTRB(127 * fem,
                                          0 * fem, 127 * fem, 0 * fem),
                                      width: double.infinity,
                                      height: 5 * fem,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5 * fem),
                                        color: Color(0xff000000),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse8Bzn (12:18)
                    left: 29 * fem,
                    top: 44 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 60 * fem,
                        height: 60 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30 * fem),
                            color: Color(0xffe2e5e8),
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(
                                'assets/page-1/images/ellipse-8-bg.png',
                              ),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse10Svi (64:616)
                    left: 29 * fem,
                    top: 204 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 60 * fem,
                        height: 60 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30 * fem),
                            color: Color(0xffe2e5e8),
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(
                                'assets/page-1/images/ellipse-10-bg.png',
                              ),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse9w6n (12:21)
                    left: 319 * fem,
                    top: 124 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 60 * fem,
                        height: 60 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30 * fem),
                            color: Color(0xffe2e5e8),
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(
                                'assets/page-1/images/ellipse-9-bg.png',
                              ),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse111sL (64:618)
                    left: 319 * fem,
                    top: 284 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 60 * fem,
                        height: 60 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30 * fem),
                            color: Color(0xffe2e5e8),
                            image: DecorationImage(
                              fit: BoxFit.contain,
                              image: AssetImage(
                                'assets/page-1/images/ellipse-11-bg.png',
                              ),
                            ),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  // Positioned(
                  //   // rectangle16uhp (39:594)
                  //   left: 9 * fem,
                  //   top: 405 * fem,
                  //   child: Align(
                  //     child: SizedBox(
                  //       width: 364 * fem,
                  //       height: 46 * fem,
                  //       child: Container(
                  //         decoration: BoxDecoration(
                  //           borderRadius: BorderRadius.circular(10 * fem),
                  //           color: Color(0xffd9d9d9),
                  //         ),
                  //         child: TextField(
                  //           decoration: InputDecoration(
                  //             border: InputBorder.none,
                  //             focusedBorder: InputBorder.none,
                  //             enabledBorder: InputBorder.none,
                  //             errorBorder: InputBorder.none,
                  //             disabledBorder: InputBorder.none,
                  //           ),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  Positioned(
                    // group10nWi (39:605)
                    left: 377 * fem,
                    top: 404 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 47 * fem,
                        height: 47 * fem,
                        child: Image.asset(
                          'assets/page-1/images/group-10.png',
                          width: 47 * fem,
                          height: 47 * fem,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ioscameratpe (39:607)
                    left: 317 * fem,
                    top: 405 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 62 * fem,
                        height: 45 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ios-camera.png',
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // eyeglassesnv2 (39:609)
                    left: 13 * fem,
                    top: 408 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 38 * fem,
                        height: 40 * fem,
                        child: Image.asset(
                          'assets/page-1/images/eye-glasses.png',
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle177Bc (64:610)
                    left: 100 * fem,
                    top: 47 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 48 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10 * fem),
                            color: Color(0xfffffdfd),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle19zWJ (64:617)
                    left: 108 * fem,
                    top: 212 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 269 * fem,
                        height: 52 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10 * fem),
                            color: Color(0xfffffdfd),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle18h9p (64:614)
                    left: 140 * fem,
                    top: 129 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 168 * fem,
                        height: 44 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10 * fem),
                            color: Color(0xfffffdfd),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle20Q4E (64:619)
                    left: 62 * fem,
                    top: 293 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 247 * fem,
                        height: 69 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10 * fem),
                            color: Color(0xfffffdfd),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // silahkanfotodanberiketerangana (64:620)
                    left: 70 * fem,
                    top: 295 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 209 * fem,
                        height: 60 * fem,
                        child: Text(
                          'Silahkan Foto dan beri keterangan anda,\nbiar nanti dari pihak kami yang akan melihat\ndi lokasi',
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 12 * ffem,
                            fontWeight: FontWeight.w100,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // adayangbisasayabanturKY (64:615)
                    left: 151 * fem,
                    top: 134 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 126 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Ada yang bisa saya bantu?',
                          textAlign: TextAlign.center,
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 12 * ffem,
                            fontWeight: FontWeight.w100,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // halloadminGu4 (64:613)
                    left: 114 * fem,
                    top: 60 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 59 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Hallo Admin',
                          textAlign: TextAlign.center,
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 12 * ffem,
                            fontWeight: FontWeight.w100,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // sayainginmengeluhkankesehatans (64:612)
                    left: 119 * fem,
                    top: 221 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 194 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Saya ingin mengeluhkan kesehatan saya!',
                          textAlign: TextAlign.center,
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 12 * ffem,
                            fontWeight: FontWeight.w100,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
