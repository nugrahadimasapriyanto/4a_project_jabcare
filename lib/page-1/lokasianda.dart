import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/halamanutama.dart';
import 'package:login_flutter/utils.dart';

class lokasianda extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // lokasiandauqx (12:876)
        width: double.infinity,
        height: 993 * fem,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Stack(
          children: [
            Positioned(
              // jabcare129t (107:699)
              left: 0 * fem,
              top: 11 * fem,
              child: Align(
                child: SizedBox(
                  width: 289 * fem,
                  height: 124 * fem,
                  child: Image.asset(
                    'assets/page-1/images/jabcare-1-UsQ.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Positioned(
              // ambulance17SE (107:701)
              left: 226 * fem,
              top: 19 * fem,
              child: Align(
                child: SizedBox(
                  width: 116 * fem,
                  height: 79 * fem,
                  child: Image.asset(
                    'assets/page-1/images/ambulance-1-NbG.png',
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Positioned(
              // layer2pbY (107:694)
              left: 5 * fem,
              top: 8.5788269043 * fem,
              child: Align(
                child: SizedBox(
                  width: 40 * fem,
                  height: 42.89 * fem,
                  child: TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => halamanutama()),
                      );
                    },
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.zero,
                    ),
                    child: Image.asset(
                      'assets/page-1/images/layer-2-KLA.png',
                      width: 40 * fem,
                      height: 42.89 * fem,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              // lokasianda7Kk (107:707)
              left: 25 * fem,
              top: 102 * fem,
              child: Align(
                child: SizedBox(
                  width: 107 * fem,
                  height: 33 * fem,
                  child: Text(
                    'Lokasi Anda',
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Catamaran',
                      fontSize: 20 * ffem,
                      fontWeight: FontWeight.w700,
                      height: 1.64 * ffem / fem,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              // rectangle39oCa (107:710)
              left: 51 * fem,
              top: 229 * fem,
              child: Align(
                child: SizedBox(
                  width: 325 * fem,
                  height: 250 * fem,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10 * fem),
                    child: Image.asset(
                      'assets/page-1/images/rectangle-39.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              // image19tUv (107:721)
              left: 36 * fem,
              top: 463 * fem,
              child: Align(
                child: SizedBox(
                  width: 355 * fem,
                  height: 447 * fem,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10 * fem),
                    child: Image.asset(
                      'assets/page-1/images/image-19.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            // Positioned(
            //   // image18zH4 (107:716)
            //   left: 226 * fem,
            //   top: 354 * fem,
            //   child: Align(
            //     child: SizedBox(
            //       width: 30 * fem,
            //       height: 30 * fem,
            //       child: Image.asset(
            //         'assets/page-1/images/image-18.png',
            //         fit: BoxFit.cover,
            //       ),
            //     ),
            //   ),
            // ),
            // Positioned(
            //   // searchbar6L6 (166:719)
            //   left: 36 * fem,
            //   top: 147 * fem,
            //   child: Container(
            //     width: 355 * fem,
            //     height: 46 * fem,
            //     decoration: BoxDecoration(
            //       boxShadow: [
            //         BoxShadow(
            //           color: Color(0x3f000000),
            //           offset: Offset(0 * fem, 4 * fem),
            //           blurRadius: 2 * fem,
            //         ),
            //       ],
            //     ),
            //     child: Align(
            //       // contentPpz (I166:719;2:44)
            //       alignment: Alignment.topCenter,
            //       child: SizedBox(
            //         width: double.infinity,
            //         height: 36 * fem,
            //         child: Container(
            //           decoration: BoxDecoration(
            //             borderRadius: BorderRadius.circular(10 * fem),
            //             color: Color(0xedfafafa),
            //           ),
            //           child: TextField(
            //             decoration: InputDecoration(
            //               border: InputBorder.none,
            //               focusedBorder: InputBorder.none,
            //               enabledBorder: InputBorder.none,
            //               errorBorder: InputBorder.none,
            //               disabledBorder: InputBorder.none,
            //               contentPadding: EdgeInsets.fromLTRB(
            //                   8 * fem, 6 * fem, 8 * fem, 1 * fem),
            //             ),
            //           ),
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            Positioned(
              // ketentuanpenggunaankebijakanpr (170:925)
              left: 99 * fem,
              top: 951 * fem,
              child: Align(
                child: SizedBox(
                  width: 230 * fem,
                  height: 29 * fem,
                  child: Text(
                    'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                    textAlign: TextAlign.center,
                    style: SafeGoogleFont(
                      'Gilda Display',
                      fontSize: 12 * ffem,
                      fontWeight: FontWeight.w400,
                      height: 1.1775 * ffem / fem,
                      color: Color(0xff000000),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
