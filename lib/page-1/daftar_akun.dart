// import 'package:flutter/material.dart';
// import 'package:login_flutter/page-1/login.dart';
// import 'package:login_flutter/utils.dart';

// class daftar_akun extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     double baseWidth = 428;
//     double fem = MediaQuery.of(context).size.width / baseWidth;
//     double ffem = fem * 0.97;
//     return SingleChildScrollView(
//       child: Container(
//         width: double.infinity,
//         child: Container(
//           // loginpaA (1:4)
//           padding: EdgeInsets.fromLTRB(14 * fem, 8 * fem, 8 * fem, 7 * fem),
//           width: double.infinity,
//           decoration: BoxDecoration(
//             color: Color(0xffffffff),
//           ),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Container(
//                 // autogroupgr4rUPp (jiqooGaHay7d27CK9gR4r)
//                 margin:
//                     EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 44 * fem),
//                 width: double.infinity,
//                 height: 567 * fem,
//                 child: Stack(
//                   children: [
//                     Positioned(
//                       // jabcare1yrN (1:5)
//                       left: 0 * fem,
//                       top: 102 * fem,
//                       child: Align(
//                         child: SizedBox(
//                           width: 399 * fem,
//                           height: 194 * fem,
//                           child: Image.asset(
//                             'assets/page-1/images/jabcare-1-iP8.png',
//                             fit: BoxFit.cover,
//                           ),
//                         ),
//                       ),
//                     ),
//                     Positioned(
//                       // selamatdatangdiV42 (1:6)
//                       left: 23 * fem,
//                       top: 123 * fem,
//                       child: Align(
//                         child: SizedBox(
//                           width: 223 * fem,
//                           height: 32 * fem,
//                           child: Text(
//                             'Silahkan Daftar Akun Terlebihi Dahulu',
//                             style: SafeGoogleFont(
//                               'Gilda Display',
//                               fontSize: 27 * ffem,
//                               fontWeight: FontWeight.w400,
//                               height: 1.1775 * ffem / fem,
//                               color: Color(0xff000000),
//                             ),
//                           ),
//                         ),
//                       ),
//                     ),
//                     Positioned(
//                       // group1WUv (1:7)
//                       left: 31 * fem,
//                       top: 362 * fem,
//                       child: Container(
//                         width: 338 * fem,
//                         height: 295 * fem,
//                         decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(3 * fem),
//                         ),
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Container(
//                               // emailpenggunaBqx (1:8)
//                               margin: EdgeInsets.fromLTRB(
//                                   1 * fem, 0 * fem, 0 * fem, 6 * fem),
//                               child: Column(
//                                 children: [
//                                   Text(
//                                     'Email Pengguna',
//                                     style: SafeGoogleFont(
//                                       'Gilda Display',
//                                       fontSize: 15 * ffem,
//                                       fontWeight: FontWeight.w400,
//                                       height: 1.1775 * ffem / fem,
//                                       color: Color(0xff000000),
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             Material(
//                               child: Container(
//                                 // autogroupqqhuiDc (jir9CsuQWKAsDMiQvqQhU)
//                                 margin: EdgeInsets.fromLTRB(
//                                     1 * fem, 0 * fem, 0 * fem, 6 * fem),
//                                 width: 337 * fem,
//                                 decoration: BoxDecoration(
//                                   border: Border.all(color: Color(0xffcfcfcf)),
//                                   color: Color(0xffececec),
//                                   borderRadius: BorderRadius.circular(3 * fem),
//                                 ),
//                                 child: TextField(
//                                   decoration: InputDecoration(
//                                     border: InputBorder.none,
//                                     focusedBorder: InputBorder.none,
//                                     enabledBorder: InputBorder.none,
//                                     errorBorder: InputBorder.none,
//                                     disabledBorder: InputBorder.none,
//                                     contentPadding: EdgeInsets.fromLTRB(
//                                         14 * fem, 12 * fem, 14 * fem, 12 * fem),
//                                     hintText: 'Contoh : adamgmail.com',
//                                     hintStyle:
//                                         TextStyle(color: Color(0xff000000)),
//                                   ),
//                                   style: SafeGoogleFont(
//                                     'Gilda Display',
//                                     fontSize: 15 * ffem,
//                                     fontWeight: FontWeight.w400,
//                                     height: 1.1775 * ffem / fem,
//                                     color: Color(0xff000000),
//                                   ),
//                                 ),
//                               ),
//                             ),
//                             Container(
//                               // masukkannamapenggunayangtelaht (1:17)
//                               margin: EdgeInsets.fromLTRB(
//                                   12 * fem, 0 * fem, 0 * fem, 18 * fem),
//                               child: Text(
//                                 'Masukkan nama pengguna yang telah terdaftar',
//                                 style: SafeGoogleFont(
//                                   'Gilda Display',
//                                   fontSize: 14 * ffem,
//                                   fontWeight: FontWeight.w400,
//                                   height: 1.1775 * ffem / fem,
//                                   color: Color(0xff000000),
//                                 ),
//                               ),
//                             ),
//                             Container(
//                               // autogroupnwxjCY6 (jirExDKzAmGohaj7UNwxJ)
//                               margin: EdgeInsets.fromLTRB(
//                                   1 * fem, 0 * fem, 0 * fem, 16 * fem),
//                               width: 73 * fem,
//                               height: 18 * fem,
//                               child: Stack(
//                                 children: [
//                                   Positioned(
//                                     // katasandi8Ar (1:9)
//                                     left: 0 * fem,
//                                     top: 0 * fem,
//                                     child: Align(
//                                       child: SizedBox(
//                                         width: 73 * fem,
//                                         height: 18 * fem,
//                                         child: Text(
//                                           'Kata Sandi',
//                                           style: SafeGoogleFont(
//                                             'Gilda Display',
//                                             fontSize: 15 * ffem,
//                                             fontWeight: FontWeight.w400,
//                                             height: 1.1775 * ffem / fem,
//                                             color: Color(0xff000000),
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                             Material(
//                               child: Container(
//                                 // autogroup3hwniuk (jirLsD98kSjw1hPf73HWN)
//                                 margin: EdgeInsets.fromLTRB(
//                                     1 * fem, 0 * fem, 0 * fem, 9 * fem),
//                                 width: double.infinity,
//                                 decoration: BoxDecoration(
//                                   border: Border.all(color: Color(0xffcfcfcf)),
//                                   color: Color(0xffececec),
//                                   borderRadius: BorderRadius.circular(3 * fem),
//                                 ),

//                                 child: TextField(
//                                   obscureText: true,
//                                   decoration: InputDecoration(
//                                     border: InputBorder.none,
//                                     focusedBorder: InputBorder.none,
//                                     enabledBorder: InputBorder.none,
//                                     errorBorder: InputBorder.none,
//                                     disabledBorder: InputBorder.none,
//                                     contentPadding: EdgeInsets.fromLTRB(
//                                         14 * fem, 6 * fem, 11 * fem, 6 * fem),
//                                     hintText: 'Kata sandi Anda',
//                                     hintStyle:
//                                         TextStyle(color: Color(0xff000000)),
//                                   ),
//                                   style: SafeGoogleFont(
//                                     'Gilda Display',
//                                     fontSize: 15 * ffem,
//                                     fontWeight: FontWeight.w400,
//                                     height: 1.1775 * ffem / fem,
//                                     color: Color(0xff000000),
//                                   ),
//                                 ),
//                               ),
//                             ),
//                           ],
//                         ),
//                       ),
//                     ),
//                     Positioned(
//                       // ambulance15nE (84:630)
//                       left: 234 * fem,
//                       top: 0 * fem,
//                       child: Align(
//                         child: SizedBox(
//                           width: 172 * fem,
//                           height: 128 * fem,
//                           child: Image.asset(
//                             'assets/page-1/images/ambulance-1-i1k.png',
//                             fit: BoxFit.cover,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Container(
//                 margin:
//                     EdgeInsets.fromLTRB(40 * fem, 0 * fem, 40 * fem, 16 * fem),
//                 width: double.infinity,
//                 height: 48 * fem,
//                 child: TextButton(
//                   onPressed: () {
//                     Navigator.push(
//                       context,
//                       MaterialPageRoute(builder: (context) => LoginPage()),
//                     );
//                   },
//                   style: TextButton.styleFrom(
//                     backgroundColor: Color.fromARGB(255, 167, 42, 42),
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(3 * fem),
//                     ),
//                   ),
//                   child: Text(
//                     'kembali',
//                     style: SafeGoogleFont(
//                       'Gilda Display',
//                       fontSize: 15 * ffem,
//                       fontWeight: FontWeight.w400,
//                       height: 1.1775 * ffem / fem,
//                       color: Color(0xffffffff),
//                     ),
//                   ),
//                 ),
//               ),
//               Container(
//                 margin:
//                     EdgeInsets.fromLTRB(40 * fem, 0 * fem, 40 * fem, 16 * fem),
//                 width: double.infinity,
//                 height: 48 * fem,
//                 child: TextButton(
//                   onPressed: () {
//                     // Navigator.push(
//                     //   context,
//                     //   MaterialPageRoute(builder: (context) => coba()),
//                     // );
//                   },
//                   style: TextButton.styleFrom(
//                     backgroundColor: Color(0xff009688),
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(3 * fem),
//                     ),
//                   ),
//                   child: Text(
//                     'Daftar Akun',
//                     style: SafeGoogleFont(
//                       'Gilda Display',
//                       fontSize: 15 * ffem,
//                       fontWeight: FontWeight.w400,
//                       height: 1.1775 * ffem / fem,
//                       color: Color(0xffffffff),
//                     ),
//                   ),
//                 ),
//               ),
//               Container(
//                 // autogroupxgywnRk (jirfwVh7qL4oZAc4hxgYW)
//                 margin:
//                     EdgeInsets.fromLTRB(44 * fem, 0 * fem, 25 * fem, 11 * fem),
//                 width: double.infinity,
//                 height: 260 * fem,
//                 child: Stack(
//                   children: [
//                     Positioned(
//                       // hospital11J9C (84:598)
//                       left: 72 * fem,
//                       top: 0 * fem,
//                       child: Align(
//                         child: SizedBox(
//                           width: 265 * fem,
//                           height: 260 * fem,
//                           child: Image.asset(
//                             'assets/page-1/images/hospital-1-1.png',
//                             fit: BoxFit.cover,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Container(
//                 // ketentuanpenggunaankebijakanpr (1:18)
//                 margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 6 * fem, 0 * fem),
//                 constraints: BoxConstraints(
//                   maxWidth: 230 * fem,
//                 ),
//                 child: Text(
//                   'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
//                   textAlign: TextAlign.center,
//                   style: SafeGoogleFont(
//                     'Gilda Display',
//                     fontSize: 12 * ffem,
//                     fontWeight: FontWeight.w400,
//                     height: 1.1775 * ffem / fem,
//                     color: Color(0xff000000),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

// Widget build(BuildContext context) {
//     return Scaffold(
//         body: SingleChildScrollView(
//       child: Padding(
//         padding: const EdgeInsets.fromLTRB(25, 40, 25, 0),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             Center(
//               child: new Image(
//                 image: AssetImage(
//                   "images/assets/image 7.png",
//                 ),
//                 width: 300,
//                 height: 200,
//               ),
//             ),
//             SizedBox(
//               height: 10.0,
//             ),
//             Text(
//               "Email",
//             ),
//             SizedBox(
//               height: 15.0,
//             ),
//             TextField(
//               controller: emailController,
//               decoration: InputDecoration(
//                 border: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(15),
//                 ),
//                 prefixIcon: Icon(
//                   Icons.email,
//                   color: Colors.black,
//                 ),
//                 hintText: "Tuliskan Email",
//               ),
//             ),
//             SizedBox(
//               height: 15.0,
//             ),
//             Text(
//               "Password",
//             ),
//             SizedBox(
//               height: 15.0,
//             ),
//             TextField(
//               controller: passwordController,
//               obscureText: true,
//               decoration: InputDecoration(
//                 border: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(15),
//                 ),
//                 prefixIcon: Icon(
//                   Icons.lock,
//                   color: Colors.black,
//                 ),
//                 hintText: "Tuliskan Password",
//                 errorText: errorValidator ? "Password tidak sesuai" : null,
//               ),
//             ),
//             SizedBox(
//               height: 15.0,
//             ),
//             Text(
//               "Validasi Password",
//             ),
//             SizedBox(
//               height: 15.0,
//             ),
//             TextField(
//               controller: passwordValidasiController,
//               obscureText: true,
//               decoration: InputDecoration(
//                 border: OutlineInputBorder(
//                   borderRadius: BorderRadius.circular(15),
//                 ),
//                 prefixIcon: Icon(
//                   Icons.lock,
//                   color: Colors.black,
//                 ),
//                 hintText: "Tuliskan Password",
//                 errorText: errorValidator ? "Password tidak sesuai" : null,
//               ),
//             ),
//             SizedBox(
//               height: 50.0,
//             ),
//             Padding(
//               padding: EdgeInsets.fromLTRB(50, 0, 0, 0),
//               child: SizedBox(
//                 width: 250.0,
//                 height: 50.0,
//                 child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(15),
//                     ),
//                   ),
//                   onPressed: () {
//                     setState(() {
//                       passwordController.text == passwordValidasiController.text
//                           ? errorValidator = false
//                           : errorValidator = true;
//                     });
//                     if (errorValidator) {
//                       print("Error");
//                     } else {
//                       signUp();
//                       Navigator.pop(context);
//                     }
//                   },
//                   child: Text("Daftar"),
//                 ),
//               ),
//             ),
//             Padding(
//               padding: EdgeInsets.fromLTRB(50, 20, 0, 0),
//               child: SizedBox(
//                 width: 250.0,
//                 height: 50.0,
//                 child: ElevatedButton(
//                   style: ElevatedButton.styleFrom(
//                     shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(15),
//                     ),
//                   ),
//                   onPressed: () {
//                     Navigator.push(
//                       context,
//                       MaterialPageRoute(
//                         builder: (context) => LoginPage(),
//                       ),
//                     );
//                   },
//                   child: Text("Batal"),
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//     ));
//   }