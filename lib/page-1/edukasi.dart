import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/halamanutama.dart';
import 'package:login_flutter/utils.dart';

class edukasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // edukasisup (12:972)
        padding: EdgeInsets.fromLTRB(0 * fem, 8.58 * fem, 0 * fem, 18 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupcnpvyT4 (jj4pWRNZXTYjrHxCNCnpv)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 86 * fem, 11 * fem),
              width: 342 * fem,
              height: 126.42 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // jabcare1Uei (607:1148)
                    left: 0 * fem,
                    top: 2.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 262 * fem,
                        height: 124 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-6wx.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1zNA (607:1149)
                    left: 226 * fem,
                    top: 10.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-QQv.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // layer26g6 (607:1154)
                    left: 5 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 40 * fem,
                        height: 42.89 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => halamanutama()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/layer-2-DNv.png',
                            width: 40 * fem,
                            height: 42.89 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // edukasikesehatanzFg (607:1158)
                    left: 25 * fem,
                    top: 93.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 166 * fem,
                        height: 33 * fem,
                        child: Text(
                          'Edukasi Kesehatan',
                          textAlign: TextAlign.center,
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 20 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupcfti4WS (jj51kbdsUWhqauVppcfti)
              margin:
                  EdgeInsets.fromLTRB(35 * fem, 0 * fem, 37 * fem, 47 * fem),
              width: double.infinity,
              height: 47 * fem,
              child: Stack(
                children: [
                  // Positioned(
                  //   // searchbarnxE (166:849)
                  //   left: 0 * fem,
                  //   top: 0 * fem,
                  //   child: Container(
                  //     width: 355 * fem,
                  //     height: 46 * fem,
                  //     decoration: BoxDecoration(
                  //       boxShadow: [
                  //         BoxShadow(
                  //           color: Color(0x3f000000),
                  //           offset: Offset(0 * fem, 4 * fem),
                  //           blurRadius: 2 * fem,
                  //         ),
                  //       ],
                  //     ),
                  //     child: Container(
                  //       // contenth3c (I166:849;2:44)
                  //       padding: EdgeInsets.fromLTRB(
                  //           8 * fem, 6 * fem, 8 * fem, 1 * fem),
                  //       width: double.infinity,
                  //       height: 36 * fem,
                  //       decoration: BoxDecoration(
                  //         color: Color(0xedfafafa),
                  //         borderRadius: BorderRadius.circular(10 * fem),
                  //       ),
                  //       child: Row(
                  //         crossAxisAlignment: CrossAxisAlignment.center,
                  //         children: [
                  //           Opacity(
                  //             // imicandroidKan (I166:849;2:45)
                  //             opacity: 0,
                  //             child: Container(
                  //               margin: EdgeInsets.fromLTRB(
                  //                   0 * fem, 0 * fem, 118.5 * fem, 5 * fem),
                  //               width: 24 * fem,
                  //               height: 24 * fem,
                  //               child: Image.asset(
                  //                 'assets/page-1/images/i-mic-android.png',
                  //                 width: 24 * fem,
                  //                 height: 24 * fem,
                  //               ),
                  //             ),
                  //           ),
                  //           Container(
                  //             // contCuU (I166:849;2:46)
                  //             margin: EdgeInsets.fromLTRB(
                  //                 0 * fem, 1 * fem, 125.5 * fem, 0 * fem),
                  //             height: 28 * fem,
                  //             child: Text(
                  //               'Search',
                  //               style: SafeGoogleFont(
                  //                 'SF Pro Text',
                  //                 fontSize: 17 * ffem,
                  //                 fontWeight: FontWeight.w400,
                  //                 height: 1.2941176471 * ffem / fem,
                  //                 letterSpacing: -0.4079999924 * fem,
                  //                 color: Color(0x993c3c43),
                  //               ),
                  //             ),
                  //           ),
                  //           Container(
                  //             // mic5iN (I166:849;2:49)
                  //             margin: EdgeInsets.fromLTRB(
                  //                 0 * fem, 0 * fem, 0 * fem, 5 * fem),
                  //             width: 24 * fem,
                  //             height: 24 * fem,
                  //             child: Image.asset(
                  //               'assets/page-1/images/mic-P82.png',
                  //               width: 24 * fem,
                  //               height: 24 * fem,
                  //             ),
                  //           ),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  // Positioned(
                  //   // searchbaroeN (606:1021)
                  //   left: 1 * fem,
                  //   top: 1 * fem,
                  //   child: Container(
                  //     width: 355 * fem,
                  //     height: 46 * fem,
                  //     decoration: BoxDecoration(
                  //       boxShadow: [
                  //         BoxShadow(
                  //           color: Color(0x3f000000),
                  //           offset: Offset(0 * fem, 4 * fem),
                  //           blurRadius: 2 * fem,
                  //         ),
                  //       ],
                  //     ),
                  //     child: Align(
                  //       // contentVn6 (I606:1021;2:44)
                  //       alignment: Alignment.topCenter,
                  //       child: SizedBox(
                  //         width: double.infinity,
                  //         height: 36 * fem,
                  //         child: Container(
                  //           decoration: BoxDecoration(
                  //             borderRadius: BorderRadius.circular(10 * fem),
                  //             color: Color(0xedfafafa),
                  //           ),
                  //           child: TextField(
                  //             decoration: InputDecoration(
                  //               border: InputBorder.none,
                  //               focusedBorder: InputBorder.none,
                  //               enabledBorder: InputBorder.none,
                  //               errorBorder: InputBorder.none,
                  //               disabledBorder: InputBorder.none,
                  //               contentPadding: EdgeInsets.fromLTRB(
                  //                   8 * fem, 6 * fem, 8 * fem, 1 * fem),
                  //             ),
                  //           ),
                  //         ),
                  //       ),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
            Container(
              // autogroupsrychNN (jj5MQhYqmhos1zVhESryC)
              margin:
                  EdgeInsets.fromLTRB(24 * fem, 0 * fem, 22 * fem, 19 * fem),
              padding:
                  EdgeInsets.fromLTRB(11 * fem, 11 * fem, 11 * fem, 2 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffd9d9d9),
                borderRadius: BorderRadius.circular(10 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // image1Ydt (84:618)
                    margin:
                        EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 4 * fem),
                    width: 360 * fem,
                    height: 150 * fem,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10 * fem),
                      child: Image.asset(
                        'assets/page-1/images/image-1.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    // kadiskesehatanpesisirbaratmemb (84:614)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 42 * fem, 0 * fem),
                    constraints: BoxConstraints(
                      maxWidth: 308 * fem,
                    ),
                    child: Text(
                      'Kadis Kesehatan Pesisir Barat Membuka Secara Resmi Acara Penyebaran Informasi Obat Di Jawa Barat',
                      style: SafeGoogleFont(
                        'Catamaran',
                        fontSize: 16 * ffem,
                        fontWeight: FontWeight.w100,
                        height: 1.64 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupwn6ausk (jj5VKe2oN9fEEnzPvWn6A)
              margin:
                  EdgeInsets.fromLTRB(24 * fem, 0 * fem, 22 * fem, 19 * fem),
              padding:
                  EdgeInsets.fromLTRB(11 * fem, 11 * fem, 11 * fem, 27 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffd9d9d9),
                borderRadius: BorderRadius.circular(10 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // image2ayt (84:622)
                    margin:
                        EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 5 * fem),
                    width: 360 * fem,
                    height: 150 * fem,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10 * fem),
                      child: Image.asset(
                        'assets/page-1/images/image-2.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    // brimobkeprimasihmenggelargerai (84:620)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 30 * fem, 0 * fem),
                    constraints: BoxConstraints(
                      maxWidth: 322 * fem,
                    ),
                    child: Text(
                      'Brimob Kepri Masih Menggelar Gerai Vaksin Guna \nDukung Kesehatan Masyarakat Secara Menyeluruh',
                      style: SafeGoogleFont(
                        'Catamaran',
                        fontSize: 16 * ffem,
                        fontWeight: FontWeight.w100,
                        height: 1.64 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupz6sy8tv (jj5cV6mCq4NFk63niZ6sY)
              margin:
                  EdgeInsets.fromLTRB(24 * fem, 0 * fem, 22 * fem, 22 * fem),
              padding:
                  EdgeInsets.fromLTRB(11 * fem, 11 * fem, 11 * fem, 26 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffd9d9d9),
                borderRadius: BorderRadius.circular(10 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // image3okA (84:628)
                    margin:
                        EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 6 * fem),
                    width: 360 * fem,
                    height: 150 * fem,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10 * fem),
                      child: Image.asset(
                        'assets/page-1/images/image-3.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    // wakilgubernurjawabaratposyandu (84:629)
                    margin:
                        EdgeInsets.fromLTRB(0 * fem, 0 * fem, 6 * fem, 0 * fem),
                    constraints: BoxConstraints(
                      maxWidth: 350 * fem,
                    ),
                    child: Text(
                      'Wakil Gubernur Jawa Barat: Posyandu, Garda Terdepan \nKesehatan Masyarakat',
                      style: SafeGoogleFont(
                        'Catamaran',
                        fontSize: 16 * ffem,
                        fontWeight: FontWeight.w100,
                        height: 1.64 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupf3gzsdY (jj5iKGP4x89neFtQof3Gz)
              margin:
                  EdgeInsets.fromLTRB(21 * fem, 0 * fem, 22 * fem, 28 * fem),
              padding:
                  EdgeInsets.fromLTRB(13 * fem, 12 * fem, 12 * fem, 18 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffe7e7e7),
                borderRadius: BorderRadius.circular(5 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Center(
                // posterviruscoronacaramemakaima (70:593)
                child: SizedBox(
                  width: 360 * fem,
                  height: 493 * fem,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5 * fem),
                    child: Image.asset(
                      'assets/gif/masker.gif',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              // autogroupvnknrVU (jj5o4dUF7C4cB8z2pVnkN)
              margin:
                  EdgeInsets.fromLTRB(21 * fem, 0 * fem, 22 * fem, 37 * fem),
              padding:
                  EdgeInsets.fromLTRB(13 * fem, 22 * fem, 12 * fem, 35 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffe7e7e7),
                borderRadius: BorderRadius.circular(5 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Center(
                // caramenjagakesehatanmentalinst (107:571)
                child: SizedBox(
                  width: 360 * fem,
                  height: 466 * fem,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5 * fem),
                    child: Image.asset(
                      'assets/gif/mental.gif',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              // autogroup1jfcSyU (jj5t4V9Ge72CwrZRU1jfc)
              margin:
                  EdgeInsets.fromLTRB(21 * fem, 0 * fem, 22 * fem, 72 * fem),
              padding:
                  EdgeInsets.fromLTRB(13 * fem, 18 * fem, 12 * fem, 25 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffe7e7e7),
                borderRadius: BorderRadius.circular(5 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Center(
                // lightyellowsimpleplayfulmanfaa (107:574)
                child: SizedBox(
                  width: 350 * fem,
                  height: 460 * fem,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5 * fem),
                    child: Image.asset(
                      'assets/gif/sayuran.gif',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              // ketentuanpenggunaankebijakanpr (70:594)
              constraints: BoxConstraints(
                maxWidth: 230 * fem,
              ),
              child: Text(
                'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Gilda Display',
                  fontSize: 12 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.1775 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
