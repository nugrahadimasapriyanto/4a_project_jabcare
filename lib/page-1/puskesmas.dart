import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/fasilitas.dart';
import 'package:login_flutter/utils.dart';

class puskesmas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // nNS (107:756)
        padding: EdgeInsets.fromLTRB(0 * fem, 8.75 * fem, 0 * fem, 14 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupgu4rHKC (jjB8aQQkKLmQKoNYkGu4r)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 72 * fem, 11 * fem),
              width: 383 * fem,
              height: 126.25 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // jabcare1CSA (608:1489)
                    left: 0 * fem,
                    top: 2.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 289 * fem,
                        height: 124 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-EpW.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1vNA (608:1490)
                    left: 240 * fem,
                    top: 22.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-AjY.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // layer2Ete (608:1492)
                    left: 5 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 40 * fem,
                        height: 43.75 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => fasilitas()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/layer-2-uMY.png',
                            width: 40 * fem,
                            height: 43.75 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            // Container(
            //   // searchbarvmU (608:1491)
            //   margin:
            //       EdgeInsets.fromLTRB(35 * fem, 0 * fem, 38 * fem, 22 * fem),
            //   width: double.infinity,
            //   height: 46 * fem,
            //   decoration: BoxDecoration(
            //     boxShadow: [
            //       BoxShadow(
            //         color: Color(0x3f000000),
            //         offset: Offset(0 * fem, 4 * fem),
            //         blurRadius: 2 * fem,
            //       ),
            //     ],
            //   ),
            //   child: Align(
            //     // contentRy8 (I608:1491;2:44)
            //     alignment: Alignment.topCenter,
            //     child: SizedBox(
            //       width: double.infinity,
            //       height: 36 * fem,
            //       child: Container(
            //         decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(10 * fem),
            //           color: Color(0xedfafafa),
            //         ),
            //         child: TextField(
            //           decoration: InputDecoration(
            //             border: InputBorder.none,
            //             focusedBorder: InputBorder.none,
            //             enabledBorder: InputBorder.none,
            //             errorBorder: InputBorder.none,
            //             disabledBorder: InputBorder.none,
            //             contentPadding: EdgeInsets.fromLTRB(
            //                 8 * fem, 6 * fem, 8 * fem, 1 * fem),
            //           ),
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            Container(
              // image28eaz (166:1125)
              margin:
                  EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 15.37 * fem),
              width: 356 * fem,
              height: 624.63 * fem,
              child: Image.asset(
                'assets/page-1/images/image-28.png',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              // autogroupg3b4mfc (jjBEEue3XBBkQ5ZKkG3B4)
              margin:
                  EdgeInsets.fromLTRB(48 * fem, 0 * fem, 55 * fem, 27 * fem),
              padding:
                  EdgeInsets.fromLTRB(16 * fem, 11 * fem, 16 * fem, 18 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xffcacaca)),
                color: Color(0xffeaeaea),
                borderRadius: BorderRadius.circular(10 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Center(
                // image24Fan (107:770)
                child: SizedBox(
                  width: 293 * fem,
                  height: 221 * fem,
                  child: TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.zero,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10 * fem),
                      child: Image.asset(
                        'assets/page-1/images/image-24-p94.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              // ketentuanpenggunaankebijakanpr (107:766)
              constraints: BoxConstraints(
                maxWidth: 230 * fem,
              ),
              child: Text(
                'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Gilda Display',
                  fontSize: 12 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.1775 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
