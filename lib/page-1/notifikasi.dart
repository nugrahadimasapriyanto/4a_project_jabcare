import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/halamanutama.dart';
import 'package:login_flutter/utils.dart';

class notifikasi extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // notifikasiZeN (84:631)
        padding: EdgeInsets.fromLTRB(0 * fem, 8.75 * fem, 0 * fem, 255 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupmogiUWS (jj91dwFu1HBexZowdmogi)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 67 * fem, 16 * fem),
              width: 388 * fem,
              height: 137.25 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // jabcare1PNW (84:633)
                    left: 0 * fem,
                    top: 2.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 289 * fem,
                        height: 124 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-b7U.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1taA (84:634)
                    left: 245 * fem,
                    top: 9.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // notifikasiox2 (84:632)
                    left: 33 * fem,
                    top: 104.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 85 * fem,
                        height: 33 * fem,
                        child: Text(
                          'Notifikasi',
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 20 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // layer2hGi (185:934)
                    left: 5 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 40 * fem,
                        height: 43.75 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => halamanutama()));
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/layer-2-DSe.png',
                            width: 40 * fem,
                            height: 43.75 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // group16o4r (607:1271)
              margin:
                  EdgeInsets.fromLTRB(15 * fem, 0 * fem, 16 * fem, 15 * fem),
              padding:
                  EdgeInsets.fromLTRB(14 * fem, 9 * fem, 20 * fem, 9 * fem),
              width: double.infinity,
              height: 71 * fem,
              decoration: BoxDecoration(
                color: Color(0xffd9d9d9),
                borderRadius: BorderRadius.circular(20 * fem),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // autogroupp1aws4i (jj9fTBajLb72Piw4fP1AW)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 20 * fem, 0 * fem),
                    width: 52 * fem,
                    height: double.infinity,
                    child: Stack(
                      children: [
                        Positioned(
                          // rectangle35PYr (607:1273)
                          left: 0 * fem,
                          top: 0 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 52 * fem,
                              height: 52 * fem,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10 * fem),
                                  border: Border.all(color: Color(0xffe6e6e6)),
                                  color: Color(0xffe6e6e6),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          // GsY (607:1278)
                          left: 18 * fem,
                          top: 0 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 17 * fem,
                              height: 53 * fem,
                              child: Text(
                                '5\n',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 32 * ffem,
                                  fontWeight: FontWeight.w700,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0xff5e5e5e),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // autogrouprktnNfg (jj9jnPhVCabsrsu4xRktn)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 9 * fem, 54 * fem, 0 * fem),
                    width: 225 * fem,
                    height: 44 * fem,
                    child: Stack(
                      children: [
                        Positioned(
                          // JZL (607:1276)
                          left: 0 * fem,
                          top: 22 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 89 * fem,
                              height: 22 * fem,
                              child: Text(
                                '05-12-2022 09:23',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 13 * ffem,
                                  fontWeight: FontWeight.w100,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0x7f000000),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          // waktuandacheckupsudahdekatCuc (607:1280)
                          left: 0 * fem,
                          top: 0 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 225 * fem,
                              height: 27 * fem,
                              child: Text(
                                'Waktu anda check up sudah dekat !',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 16 * ffem,
                                  fontWeight: FontWeight.w100,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0xff000000),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // chevrondown5yQ (607:1282)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 20.14 * fem),
                    width: 12 * fem,
                    height: 5.14 * fem,
                    child: Image.asset(
                      'assets/page-1/images/chevron-down-kH4.png',
                      width: 12 * fem,
                      height: 5.14 * fem,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // group13C2S (84:674)
              margin:
                  EdgeInsets.fromLTRB(15 * fem, 0 * fem, 16 * fem, 15 * fem),
              padding:
                  EdgeInsets.fromLTRB(14 * fem, 19 * fem, 20 * fem, 20 * fem),
              width: double.infinity,
              height: 137 * fem,
              decoration: BoxDecoration(
                color: Color(0xffd9d9d9),
                borderRadius: BorderRadius.circular(20 * fem),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // image5Hpa (84:722)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 19 * fem, 0 * fem),
                    width: 97 * fem,
                    height: 98 * fem,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10 * fem),
                      child: Image.asset(
                        'assets/page-1/images/image-5.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    // autogroupudveQuC (jj9HTpDpCAtPQAoM1udve)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 16 * fem, 21 * fem, 8 * fem),
                    width: 214 * fem,
                    height: double.infinity,
                    child: Stack(
                      children: [
                        Positioned(
                          // implementasigermaspemkotdepokv (84:677)
                          left: 0 * fem,
                          top: 0 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 214 * fem,
                              height: 53 * fem,
                              child: Text(
                                'Implementasi Germas, Pemkot Depok ...',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 16 * ffem,
                                  fontWeight: FontWeight.w700,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0xff434343),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          // Dbk (84:678)
                          left: 0 * fem,
                          top: 52 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 89 * fem,
                              height: 22 * fem,
                              child: Text(
                                '05-12-2022 06:23',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 13 * ffem,
                                  fontWeight: FontWeight.w100,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0x7f000000),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // icon7SE (84:740)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 71.9 * fem),
                    width: 12 * fem,
                    height: 6.1 * fem,
                    child: Image.asset(
                      'assets/page-1/images/icon.png',
                      width: 12 * fem,
                      height: 6.1 * fem,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // group17q7L (607:1286)
              margin:
                  EdgeInsets.fromLTRB(18 * fem, 0 * fem, 13 * fem, 10 * fem),
              padding:
                  EdgeInsets.fromLTRB(14 * fem, 9 * fem, 20 * fem, 9 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffd9d9d9),
                borderRadius: BorderRadius.circular(20 * fem),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // autogrouplrv8igv (jj9xShGcx6N4cZVPfLRV8)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 20 * fem, 1 * fem),
                    width: 52 * fem,
                    height: 52 * fem,
                    child: Image.asset(
                      'assets/page-1/images/auto-group-lrv8.png',
                      width: 52 * fem,
                      height: 52 * fem,
                    ),
                  ),
                  Container(
                    // autogroup5xzrpzr (jjA3msinKU4334NUV5xzr)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 9 * fem, 47 * fem, 0 * fem),
                    width: 232 * fem,
                    height: 44 * fem,
                    child: Stack(
                      children: [
                        Positioned(
                          // 9nE (607:1289)
                          left: 0 * fem,
                          top: 22 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 89 * fem,
                              height: 22 * fem,
                              child: Text(
                                '05-12-2022 09:23',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 13 * ffem,
                                  fontWeight: FontWeight.w100,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0x7f000000),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          // andatelahmelakukanpendaftaranf (607:1295)
                          left: 0 * fem,
                          top: 0 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 232 * fem,
                              height: 27 * fem,
                              child: Text(
                                'Anda telah melakukan pendaftaran ...',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 16 * ffem,
                                  fontWeight: FontWeight.w100,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0xff000000),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // chevrondown9Qr (607:1292)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 20.14 * fem),
                    width: 12 * fem,
                    height: 5.14 * fem,
                    child: Image.asset(
                      'assets/page-1/images/chevron-down.png',
                      width: 12 * fem,
                      height: 5.14 * fem,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // group18Suk (607:1300)
              margin:
                  EdgeInsets.fromLTRB(18 * fem, 0 * fem, 13 * fem, 15 * fem),
              padding:
                  EdgeInsets.fromLTRB(14 * fem, 9 * fem, 20 * fem, 9 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Color(0xffd9d9d9),
                borderRadius: BorderRadius.circular(20 * fem),
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // autogroupm2uvwLi (jjAGwATcpfunGQvLTM2Uv)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 20 * fem, 1 * fem),
                    width: 52 * fem,
                    height: 52 * fem,
                    child: Image.asset(
                      'assets/page-1/images/auto-group-m2uv.png',
                      width: 52 * fem,
                      height: 52 * fem,
                    ),
                  ),
                  Container(
                    // autogroupryqaSoG (jjAMw28eMasP38Vj6ryQA)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 9 * fem, 94 * fem, 0 * fem),
                    width: 185 * fem,
                    height: 44 * fem,
                    child: Stack(
                      children: [
                        Positioned(
                          // akunandasudahterverifikasiNS2 (607:1313)
                          left: 0 * fem,
                          top: 0 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 185 * fem,
                              height: 27 * fem,
                              child: Text(
                                'Akun anda sudah terverifikasi',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 16 * ffem,
                                  fontWeight: FontWeight.w100,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0xff000000),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          // TCa (607:1314)
                          left: 0 * fem,
                          top: 22 * fem,
                          child: Align(
                            child: SizedBox(
                              width: 87 * fem,
                              height: 22 * fem,
                              child: Text(
                                '04-12-2022 10:23',
                                style: SafeGoogleFont(
                                  'Catamaran',
                                  fontSize: 13 * ffem,
                                  fontWeight: FontWeight.w100,
                                  height: 1.64 * ffem / fem,
                                  color: Color(0x7f000000),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // chevrondownLnA (607:1305)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 20.14 * fem),
                    width: 12 * fem,
                    height: 5.14 * fem,
                    child: Image.asset(
                      'assets/page-1/images/chevron-down-ydg.png',
                      width: 12 * fem,
                      height: 5.14 * fem,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // group15enr (84:702)
              margin: EdgeInsets.fromLTRB(15 * fem, 0 * fem, 16 * fem, 0 * fem),
              padding:
                  EdgeInsets.fromLTRB(14 * fem, 19 * fem, 20 * fem, 23 * fem),
              width: double.infinity,
              height: 140 * fem,
              decoration: BoxDecoration(
                color: Color(0xffd9d9d9),
                borderRadius: BorderRadius.circular(20 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // image4WaA (84:715)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 19 * fem, 0 * fem),
                    width: 97 * fem,
                    height: 98 * fem,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10 * fem),
                      child: Image.asset(
                        'assets/page-1/images/image-4.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    // autogroupwxm8Cxn (jj9TTXZsFzoawbx8JwXm8)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 15 * fem, 36 * fem, 5 * fem),
                    height: double.infinity,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          // puskesmasjawabaratmilikigedung (84:710)
                          margin: EdgeInsets.fromLTRB(
                              0 * fem, 0 * fem, 0 * fem, 3 * fem),
                          constraints: BoxConstraints(
                            maxWidth: 199 * fem,
                          ),
                          child: Text(
                            'Puskesmas Jawa Barat Miliki \nGedung ...',
                            style: SafeGoogleFont(
                              'Catamaran',
                              fontSize: 16 * ffem,
                              fontWeight: FontWeight.w700,
                              height: 1.64 * ffem / fem,
                              color: Color(0xff434343),
                            ),
                          ),
                        ),
                        Text(
                          // QZ4 (84:716)
                          '03-12-2022 03:23',
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 13 * ffem,
                            fontWeight: FontWeight.w100,
                            height: 1.64 * ffem / fem,
                            color: Color(0x7f000000),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    // iconwot (84:742)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 67.9 * fem),
                    width: 12 * fem,
                    height: 6.1 * fem,
                    child: Image.asset(
                      'assets/page-1/images/icon-YLv.png',
                      width: 12 * fem,
                      height: 6.1 * fem,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
