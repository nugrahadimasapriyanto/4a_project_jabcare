import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/apotek.dart';
import 'package:login_flutter/page-1/bpjs.dart';
import 'package:login_flutter/page-1/halamanutama.dart';
import 'package:login_flutter/page-1/puskesmas.dart';
import 'package:login_flutter/page-1/rumahsakit.dart';
import 'package:login_flutter/utils.dart';
import 'ambulan.dart';
import 'dokter.dart';

class fasilitas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // fasilitas81U (31:1979)
        padding: EdgeInsets.fromLTRB(0 * fem, 8.58 * fem, 0 * fem, 17 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupe3n6div (jj7yAg1srdauEHwicE3n6)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 33 * fem, 34 * fem),
              width: 422 * fem,
              height: 732.42 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // cekfasilitasvT8 (31:1985)
                    left: 129 * fem,
                    top: 125.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 170 * fem,
                        height: 53 * fem,
                        child: Text(
                          'Cek Fasilitas',
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 32 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle25CQe (84:580)
                    left: 26 * fem,
                    top: 177.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 369 * fem,
                        height: 555 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10 * fem),
                            border: Border.all(color: Color(0x7fbcbcbc)),
                            color: Color(0xfff6f6f6),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // bpjsFNv (84:593)
                    left: 278 * fem,
                    top: 659.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 28 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Bpjs',
                          style: SafeGoogleFont(
                            'Buenard',
                            fontSize: 15 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // layanandaruratwmY (84:591)
                    left: 87 * fem,
                    top: 659.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 107 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Layanan Darurat',
                          style: SafeGoogleFont(
                            'Buenard',
                            fontSize: 15 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // dokterspesialis3Jn (84:589)
                    left: 238 * fem,
                    top: 484.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 107 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Dokter  Spesialis',
                          style: SafeGoogleFont(
                            'Buenard',
                            fontSize: 15 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // apotekivi (84:588)
                    left: 118 * fem,
                    top: 484.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 46 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Apotek',
                          style: SafeGoogleFont(
                            'Buenard',
                            fontSize: 15 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle261ev (84:581)
                    left: 99 * fem,
                    top: 218.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 82 * fem,
                        height: 77 * fem,
                        child: TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10 * fem),
                              border: Border.all(color: Color(0x7fbcbcbc)),
                              color: Color(0xffd9d9d9),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 2 * fem,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle28h1x (84:583)
                    left: 99 * fem,
                    top: 393.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 82 * fem,
                        height: 77 * fem,
                        child: TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10 * fem),
                              border: Border.all(color: Color(0x7fbcbcbc)),
                              color: Color(0xffd9d9d9),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 2 * fem,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle30N86 (84:590)
                    left: 99 * fem,
                    top: 568.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 82 * fem,
                        height: 77 * fem,
                        child: TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10 * fem),
                              border: Border.all(color: Color(0x7fbcbcbc)),
                              color: Color(0xffd9d9d9),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 2 * fem,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle27e5c (84:582)
                    left: 250 * fem,
                    top: 218.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 82 * fem,
                        height: 77 * fem,
                        child: TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10 * fem),
                              border: Border.all(color: Color(0x7fbcbcbc)),
                              color: Color(0xffd9d9d9),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 2 * fem,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle296yC (84:584)
                    left: 249 * fem,
                    top: 393.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 82 * fem,
                        height: 77 * fem,
                        child: TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10 * fem),
                              border: Border.all(color: Color(0x7fbcbcbc)),
                              color: Color(0xffd9d9d9),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 2 * fem,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle31adU (84:592)
                    left: 249 * fem,
                    top: 568.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 82 * fem,
                        height: 77 * fem,
                        child: TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10 * fem),
                              border: Border.all(color: Color(0x7fbcbcbc)),
                              color: Color(0xffd9d9d9),
                              boxShadow: [
                                BoxShadow(
                                  color: Color(0x3f000000),
                                  offset: Offset(0 * fem, 4 * fem),
                                  blurRadius: 2 * fem,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rumahsakitrqt (84:586)
                    left: 101 * fem,
                    top: 309.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 83 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Rumah Sakit',
                          style: SafeGoogleFont(
                            'Buenard',
                            fontSize: 15 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // puskesmasa1C (84:587)
                    left: 255 * fem,
                    top: 309.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 69 * fem,
                        height: 20 * fem,
                        child: Text(
                          'Puskesmas',
                          style: SafeGoogleFont(
                            'Buenard',
                            fontSize: 15 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.3 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // image6HRQ (107:636)
                    left: 115 * fem,
                    top: 230.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => rumahsakit()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/image-6.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // image75s4 (107:638)
                    left: 265 * fem,
                    top: 231.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => puskesmas()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/image-7.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // image8QeS (107:640)
                    left: 116 * fem,
                    top: 580.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ambulan()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/image-8.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // image9vMt (107:642)
                    left: 109 * fem,
                    top: 406.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => apotek()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/image-9.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // image102Qv (107:644)
                    left: 265 * fem,
                    top: 406.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => dokter()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/image-10.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // image11YeA (107:646)
                    left: 265 * fem,
                    top: 582.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => bpjs()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/image-11.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // jabcare1sAe (607:1181)
                    left: 0 * fem,
                    top: 2.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 289 * fem,
                        height: 124 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1aqk (607:1182)
                    left: 241 * fem,
                    top: 9.4211730957 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-nri.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // layer2ut2 (607:1187)
                    left: 5 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 40 * fem,
                        height: 42.89 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => halamanutama()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/layer-2-24n.png',
                            width: 40 * fem,
                            height: 42.89 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // postinganmediasosialpenjelasan (84:600)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 1 * fem, 14 * fem),
              width: 369 * fem,
              height: 392 * fem,
              child: Image.asset(
                'assets/gif/sosial.gif',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              // ketentuanpenggunaankebijakanpr (107:634)
              margin: EdgeInsets.fromLTRB(22 * fem, 0 * fem, 0 * fem, 0 * fem),
              constraints: BoxConstraints(
                maxWidth: 230 * fem,
              ),
              child: Text(
                'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Gilda Display',
                  fontSize: 12 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.1775 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
