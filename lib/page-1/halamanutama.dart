import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/chat.dart';
import 'package:login_flutter/page-1/edukasi.dart';
import 'package:login_flutter/page-1/exit.dart';
import 'package:login_flutter/page-1/fasilitas.dart';
import 'package:login_flutter/page-1/lokasianda.dart';
import 'package:login_flutter/page-1/notifikasi.dart';
import 'package:login_flutter/page-1/presentasi.dart';
import 'package:login_flutter/utils.dart';

class halamanutama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      child: Container(
        width: double.infinity,
        // child: Container(
        // halamanutamadQz (1:99)
        padding: EdgeInsets.fromLTRB(12 * fem, 6 * fem, 19 * fem, 21 * fem),
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupjs3pWjg (jisQFcXGwrrDHNM37jS3p)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 24 * fem),
              width: double.infinity,
              height: 344 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // jabcare11gS (1:100)
                    left: 0 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 245 * fem,
                        height: 116 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-38r.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // bellKSE (1:122)
                    left: 347 * fem,
                    top: 111 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => notifikasi()));
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/bell-BMg.png',
                            width: 50 * fem,
                            height: 50 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance11K4 (84:603)
                    left: 245 * fem,
                    top: 17 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-WpW.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // jF4 (601:936)
                    left: 66 * fem,
                    top: 120 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 274 * fem,
                        height: 224 * fem,
                        child: Image.asset(
                          'assets/gif/mental.gif',
                          // fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            // Container(
            //   // searchbarr4n (166:897)
            //   margin:
            //       EdgeInsets.fromLTRB(25 * fem, 0 * fem, 17 * fem, 38 * fem),
            //   width: double.infinity,
            //   height: 46 * fem,
            //   decoration: BoxDecoration(
            //     boxShadow: [
            //       BoxShadow(
            //         color: Color(0x3f000000),
            //         offset: Offset(0 * fem, 4 * fem),
            //         blurRadius: 2 * fem,
            //       ),
            //     ],
            //   ),
            //   child: Align(
            //     // contentw6E (I166:897;2:44)
            //     alignment: Alignment.topCenter,
            //     child: SizedBox(
            //       width: double.infinity,
            //       height: 36 * fem,
            //       child: Container(
            //         decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(10 * fem),
            //           color: Color(0xedfafafa),
            //         ),
            //         child: TextField(
            //           decoration: InputDecoration(
            //             border: InputBorder.none,
            //             focusedBorder: InputBorder.none,
            //             enabledBorder: InputBorder.none,
            //             errorBorder: InputBorder.none,
            //             disabledBorder: InputBorder.none,
            //             contentPadding: EdgeInsets.fromLTRB(
            //                 8 * fem, 6 * fem, 8 * fem, 1 * fem),
            //           ),
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            Container(
              // autogrouprxqxuxA (jisZfWWLoNzFwBiMbrXqx)
              margin: EdgeInsets.fromLTRB(41 * fem, 0 * fem, 33 * fem, 4 * fem),
              width: double.infinity,
              height: 224 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // ellipse1232n (70:577)
                    left: 11 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse157oL (70:580)
                    left: 13 * fem,
                    top: 133 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse16Q1k (70:581)
                    left: 137 * fem,
                    top: 133 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse17U1c (70:582)
                    left: 262 * fem,
                    top: 133 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse13LpW (70:578)
                    left: 262 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse14Rb4 (70:579)
                    left: 137 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // group5hYa (1:110)
                    left: 0 * fem,
                    top: 17 * fem,
                    child: Container(
                      width: 323 * fem,
                      height: 207 * fem,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5 * fem),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            // autogroup2gsgoLi (jisxerCfTBQ1t5m9n2gsG)
                            width: double.infinity,
                            height: 73 * fem,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                TextButton(
                                  // autogroupppftL5k (jit8Q4xs9AGSBgSASPPFt)
                                  onPressed: () {},
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(
                                        7 * fem, 3 * fem, 6 * fem, 3 * fem),
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Color(0xffd9d9d9),
                                      borderRadius:
                                          BorderRadius.circular(5 * fem),
                                    ),
                                    child: Align(
                                      // userfolderDfL (1:138)
                                      alignment: Alignment.topCenter,
                                      child: SizedBox(
                                        width: 60 * fem,
                                        height: 60 * fem,
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => chat()),
                                            );
                                          },
                                          style: TextButton.styleFrom(
                                            padding: EdgeInsets.zero,
                                          ),
                                          child: Image.asset(
                                            'assets/page-1/images/user-folder-TWN.png',
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 52 * fem,
                                ),
                                TextButton(
                                  // autogroupnxstJgn (jitDEGFKkqrr8WMhznXst)
                                  onPressed: () {},
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(
                                        12 * fem, 8 * fem, 11 * fem, 15 * fem),
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Color(0xffd9d9d9),
                                      borderRadius:
                                          BorderRadius.circular(5 * fem),
                                    ),
                                    child: Center(
                                      // image12Dok (107:652)
                                      child: SizedBox(
                                        width: 50 * fem,
                                        height: 50 * fem,
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      lokasianda()),
                                            );
                                          },
                                          style: TextButton.styleFrom(
                                            padding: EdgeInsets.zero,
                                          ),
                                          child: Image.asset(
                                            'assets/page-1/images/image-12.png',
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 52 * fem,
                                ),
                                TextButton(
                                  // autogroupvvzp8fp (jitGyf16QXaYi3YFUvVZp)
                                  onPressed: () {},
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(
                                        7 * fem, 3 * fem, 6 * fem, 3 * fem),
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Color(0xffd9d9d9),
                                      borderRadius:
                                          BorderRadius.circular(5 * fem),
                                    ),
                                    child: Align(
                                      // idearLv (1:140)
                                      alignment: Alignment.topCenter,
                                      child: SizedBox(
                                        width: 60 * fem,
                                        height: 60 * fem,
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      edukasi()),
                                            );
                                          },
                                          style: TextButton.styleFrom(
                                            padding: EdgeInsets.zero,
                                          ),
                                          child: Image.asset(
                                            'assets/page-1/images/idea-3xA.png',
                                            fit: BoxFit.contain,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            // autogroup8okcyRY (jitRojdB3jwSUF87A8oKc)
                            margin: EdgeInsets.fromLTRB(
                                11 * fem, 0 * fem, 11 * fem, 44 * fem),
                            width: double.infinity,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  // keluhkan71x (12:12)
                                  margin: EdgeInsets.fromLTRB(
                                      0 * fem, 0 * fem, 68 * fem, 0 * fem),
                                  child: Text(
                                    'Keluhkan',
                                    style: SafeGoogleFont(
                                      'Buenard',
                                      fontSize: 12 * ffem,
                                      fontWeight: FontWeight.w700,
                                      height: 1.3 * ffem / fem,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ),
                                Container(
                                  // lokasianda1d8 (12:13)
                                  margin: EdgeInsets.fromLTRB(
                                      0 * fem, 1 * fem, 78 * fem, 0 * fem),
                                  child: Text(
                                    'Lokasi Anda',
                                    style: SafeGoogleFont(
                                      'Buenard',
                                      fontSize: 12 * ffem,
                                      fontWeight: FontWeight.w700,
                                      height: 1.3 * ffem / fem,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ),
                                Text(
                                  // edukasi7gA (12:14)
                                  'Edukasi',
                                  style: SafeGoogleFont(
                                    'Buenard',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w700,
                                    height: 1.3 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            // autogroupcisafBt (jitZig78eBnoh3corCiSa)
                            width: double.infinity,
                            height: 73 * fem,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                TextButton(
                                  // autogrouposnazV4 (jitgxy2pZiBRcHW8BoSNa)
                                  onPressed: () {},
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(
                                        13 * fem, 16 * fem, 10 * fem, 7 * fem),
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Color(0xffd9d9d9),
                                      borderRadius:
                                          BorderRadius.circular(5 * fem),
                                    ),
                                    child: Center(
                                      // image13hPU (107:654)
                                      child: SizedBox(
                                        width: 50 * fem,
                                        height: 50 * fem,
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      presentasi()),
                                            );
                                          },
                                          style: TextButton.styleFrom(
                                            padding: EdgeInsets.zero,
                                          ),
                                          child: Image.asset(
                                            'assets/page-1/images/image-13-cCW.png',
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 52 * fem,
                                ),
                                TextButton(
                                  // autogroupvod8oBc (jitkoBysg1aibmWbDVoD8)
                                  onPressed: () {},
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(
                                        12 * fem, 16 * fem, 11 * fem, 7 * fem),
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Color(0xffd9d9d9),
                                      borderRadius:
                                          BorderRadius.circular(5 * fem),
                                    ),
                                    child: Center(
                                      // image14Ju4 (107:656)
                                      child: SizedBox(
                                        width: 50 * fem,
                                        height: 50 * fem,
                                        child: TextButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      fasilitas()),
                                            );
                                          },
                                          style: TextButton.styleFrom(
                                            padding: EdgeInsets.zero,
                                          ),
                                          child: Image.asset(
                                            'assets/page-1/images/image-14.png',
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 52 * fem,
                                ),
                                TextButton(
                                  // autogroupyywqQx6 (jitq8Q6dY15a4vUbWYYwQ)
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => exit()),
                                    );
                                  },
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.zero,
                                  ),
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(
                                        12 * fem, 12 * fem, 11 * fem, 11 * fem),
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      color: Color(0xffd9d9d9),
                                      borderRadius:
                                          BorderRadius.circular(5 * fem),
                                    ),
                                    child: Center(
                                      // image16XG2 (107:668)
                                      child: SizedBox(
                                        width: 50 * fem,
                                        height: 50 * fem,
                                        child: Image.asset(
                                          'assets/page-1/images/image-16-RRg.png',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroup7rai4mk (jiuJww5QoV5jxeSwF7rai)
              margin:
                  EdgeInsets.fromLTRB(52 * fem, 0 * fem, 52 * fem, 35 * fem),
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // presentasioUS (12:15)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 66 * fem, 0 * fem),
                    child: Text(
                      'Presentasi',
                      style: SafeGoogleFont(
                        'Buenard',
                        fontSize: 12 * ffem,
                        fontWeight: FontWeight.w700,
                        height: 1.3 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                  Container(
                    // cekfasilitas7V8 (12:16)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 75 * fem, 0 * fem),
                    child: Text(
                      'Cek Fasilitas',
                      style: SafeGoogleFont(
                        'Buenard',
                        fontSize: 12 * ffem,
                        fontWeight: FontWeight.w700,
                        height: 1.3 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                  Text(
                    // keluarRki (12:17)
                    'Keluar',
                    style: SafeGoogleFont(
                      'Buenard',
                      fontSize: 12 * ffem,
                      fontWeight: FontWeight.w700,
                      height: 1.3 * ffem / fem,
                      color: Color(0xff000000),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // group11AiJ (70:573)
              margin: EdgeInsets.fromLTRB(8 * fem, 0 * fem, 0 * fem, 56 * fem),
              width: 385 * fem,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // autogroupraoyhiE (jiv5LpmfPPvz1UqzFraoY)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 23 * fem),
                    padding: EdgeInsets.fromLTRB(
                        15 * fem, 14 * fem, 17 * fem, 18 * fem),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Color(0xffe7e7e7),
                      borderRadius: BorderRadius.circular(5 * fem),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x3f000000),
                          offset: Offset(0 * fem, 4 * fem),
                          blurRadius: 2 * fem,
                        ),
                      ],
                    ),
                    child: Center(
                      // birudanhijauillustrasiselamath (70:568)
                      child: SizedBox(
                        width: 353 * fem,
                        height: 562 * fem,
                        child: Image.asset(
                          'assets/gif/1.gif',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    // autogroupe9bkWfg (jiv8bEMjHPZ87M4zUe9bk)
                    padding: EdgeInsets.fromLTRB(
                        16 * fem, 23 * fem, 15 * fem, 25 * fem),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Color(0xffe7e7e7),
                      borderRadius: BorderRadius.circular(5 * fem),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x3f000000),
                          offset: Offset(0 * fem, 4 * fem),
                          blurRadius: 2 * fem,
                        ),
                      ],
                    ),
                    child: Center(
                      // kremdanorenilustrasicaracepatd (70:572)
                      child: SizedBox(
                        width: 354 * fem,
                        height: 396 * fem,
                        child: Image.asset(
                          'assets/gif/2.gif',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // ketentuanpenggunaankebijakanpr (1:121)
              margin: EdgeInsets.fromLTRB(9 * fem, 0 * fem, 0 * fem, 0 * fem),
              constraints: BoxConstraints(
                maxWidth: 230 * fem,
              ),
              child: Text(
                'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Gilda Display',
                  fontSize: 12 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.1775 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
