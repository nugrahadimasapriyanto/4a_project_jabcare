import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/fasilitas.dart';
import 'package:login_flutter/utils.dart';

class apotek extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // x78 (107:773)
        padding: EdgeInsets.fromLTRB(0 * fem, 8.75 * fem, 0 * fem, 18 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupfnnrGNi (jjBdZa7W1SKszkup6fnnr)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 72 * fem, 11 * fem),
              width: 383 * fem,
              height: 126.25 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // jabcare1ynv (608:1547)
                    left: 0 * fem,
                    top: 2.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 289 * fem,
                        height: 124 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-r9t.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1H2v (608:1548)
                    left: 240 * fem,
                    top: 22.2502441406 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-H7C.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // layer2nkN (608:1550)
                    left: 5 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 40 * fem,
                        height: 43.75 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => fasilitas()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/layer-2-xgE.png',
                            width: 40 * fem,
                            height: 43.75 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            // Container(
            //   // searchbargar (608:1549)
            //   margin:
            //       EdgeInsets.fromLTRB(35 * fem, 0 * fem, 38 * fem, 30 * fem),
            //   width: double.infinity,
            //   height: 46 * fem,
            //   decoration: BoxDecoration(
            //     boxShadow: [
            //       BoxShadow(
            //         color: Color(0x3f000000),
            //         offset: Offset(0 * fem, 4 * fem),
            //         blurRadius: 2 * fem,
            //       ),
            //     ],
            //   ),
            //   child: Align(
            //     // contentQFx (I608:1549;2:44)
            //     alignment: Alignment.topCenter,
            //     child: SizedBox(
            //       width: double.infinity,
            //       height: 36 * fem,
            //       child: Container(
            //         decoration: BoxDecoration(
            //           borderRadius: BorderRadius.circular(10 * fem),
            //           color: Color(0xedfafafa),
            //         ),
            //         child: TextField(
            //           decoration: InputDecoration(
            //             border: InputBorder.none,
            //             focusedBorder: InputBorder.none,
            //             enabledBorder: InputBorder.none,
            //             errorBorder: InputBorder.none,
            //             disabledBorder: InputBorder.none,
            //             contentPadding: EdgeInsets.fromLTRB(
            //                 8 * fem, 6 * fem, 8 * fem, 1 * fem),
            //           ),
            //         ),
            //       ),
            //     ),
            //   ),
            // ),
            Container(
              // image29QQN (166:1024)
              margin: EdgeInsets.fromLTRB(4 * fem, 0 * fem, 0 * fem, 19 * fem),
              width: 362 * fem,
              height: 575 * fem,
              child: Image.asset(
                'assets/page-1/images/image-29.png',
                fit: BoxFit.cover,
              ),
            ),
            Container(
              // autogroup6xks85U (jjBj9F9Wkf4df6GfZ6XkS)
              margin:
                  EdgeInsets.fromLTRB(53 * fem, 0 * fem, 50 * fem, 20 * fem),
              padding:
                  EdgeInsets.fromLTRB(16 * fem, 11 * fem, 16 * fem, 18 * fem),
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(color: Color(0xffcacaca)),
                color: Color(0xffeaeaea),
                borderRadius: BorderRadius.circular(10 * fem),
                boxShadow: [
                  BoxShadow(
                    color: Color(0x3f000000),
                    offset: Offset(0 * fem, 4 * fem),
                    blurRadius: 2 * fem,
                  ),
                ],
              ),
              child: Center(
                // image24CLE (107:783)
                child: SizedBox(
                  width: 293 * fem,
                  height: 221 * fem,
                  child: TextButton(
                    onPressed: () {},
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.zero,
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10 * fem),
                      child: Image.asset(
                        'assets/page-1/images/image-24-C9t.png',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              // ketentuanpenggunaankebijakanpr (107:781)
              margin: EdgeInsets.fromLTRB(4 * fem, 0 * fem, 0 * fem, 0 * fem),
              constraints: BoxConstraints(
                maxWidth: 230 * fem,
              ),
              child: Text(
                'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Gilda Display',
                  fontSize: 12 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.1775 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
