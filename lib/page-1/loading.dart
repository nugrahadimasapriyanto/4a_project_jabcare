import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/login.dart';

class loading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double screenWidth = MediaQuery.of(context).size.width;
    double fem = screenWidth / baseWidth;
    double ffem = fem * 0.97;

    return Container(
      width: double.infinity,
      child: Container(
        padding:
            EdgeInsets.fromLTRB(15 * ffem, 366 * ffem, 14 * ffem, 366 * ffem),
        width: double.infinity,
        height: 926 * ffem,
        decoration: BoxDecoration(
          color: Color(0xff7c69d6),
        ),
        child: Center(
          child: SizedBox(
            width: 399 * ffem,
            height: 194 * ffem,
            child: TextButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              },
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
              ),
              child: Container(
                width: 500 * ffem,
                height: 500 * ffem,
                child: Image.asset(
                  'assets/page-1/images/Play.gif',
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
