import 'package:flutter/material.dart';
import 'package:login_flutter/page-1/login.dart';
import 'package:login_flutter/utils.dart';

class exit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double baseWidth = 428;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return SingleChildScrollView(
      // width: double.infinity,
      child: Container(
        // exit2kN (607:1193)
        padding: EdgeInsets.fromLTRB(12 * fem, 6 * fem, 19 * fem, 21 * fem),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // autogroupnakgKzN (jivjjtTUw1dWLFka6NaKg)
              margin: EdgeInsets.fromLTRB(0 * fem, 0 * fem, 0 * fem, 4 * fem),
              width: double.infinity,
              height: 676 * fem,
              child: Stack(
                children: [
                  Positioned(
                    // ellipse12QW2 (607:1194)
                    left: 52 * fem,
                    top: 452 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse153ot (607:1195)
                    left: 54 * fem,
                    top: 585 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse169M8 (607:1196)
                    left: 178 * fem,
                    top: 585 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse17RpS (607:1197)
                    left: 303 * fem,
                    top: 585 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse13Kev (607:1198)
                    left: 303 * fem,
                    top: 452 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ellipse14oa6 (607:1199)
                    left: 178 * fem,
                    top: 452 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25 * fem),
                            color: Color(0xffc4c4c4),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // jabcare1WUW (607:1200)
                    left: 0 * fem,
                    top: 0 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 245 * fem,
                        height: 116 * fem,
                        child: Image.asset(
                          'assets/page-1/images/jabcare-1-d2N.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // group5SNA (607:1202)
                    left: 41 * fem,
                    top: 469 * fem,
                    child: Container(
                      width: 323 * fem,
                      height: 207 * fem,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5 * fem),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            // autogroupjsmcKwk (jiwGoq2L6VFpKqwv3jSmC)
                            width: double.infinity,
                            height: 73 * fem,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  // autogrouppmnnG6J (jiwRZ5T8H5w7g6hrBPMNN)
                                  padding: EdgeInsets.fromLTRB(
                                      7 * fem, 3 * fem, 6 * fem, 3 * fem),
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Color(0xffd9d9d9),
                                    borderRadius:
                                        BorderRadius.circular(5 * fem),
                                  ),
                                  child: Align(
                                    // userfoldermYr (607:1214)
                                    alignment: Alignment.topCenter,
                                    child: SizedBox(
                                      width: 60 * fem,
                                      height: 60 * fem,
                                      child: TextButton(
                                        onPressed: () {},
                                        style: TextButton.styleFrom(
                                          padding: EdgeInsets.zero,
                                        ),
                                        child: Image.asset(
                                          'assets/page-1/images/user-folder.png',
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 52 * fem,
                                ),
                                Container(
                                  // autogroupe7mywLr (jiwVU8bTr1215XYEke7MY)
                                  padding: EdgeInsets.fromLTRB(
                                      12 * fem, 8 * fem, 11 * fem, 15 * fem),
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Color(0xffd9d9d9),
                                    borderRadius:
                                        BorderRadius.circular(5 * fem),
                                  ),
                                  child: Center(
                                    // image124Aa (607:1229)
                                    child: SizedBox(
                                      width: 50 * fem,
                                      height: 50 * fem,
                                      child: TextButton(
                                        onPressed: () {},
                                        style: TextButton.styleFrom(
                                          padding: EdgeInsets.zero,
                                        ),
                                        child: Image.asset(
                                          'assets/page-1/images/image-12-dVx.png',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 52 * fem,
                                ),
                                Container(
                                  // autogrouprg9kADc (jiwYiYBXjze9BPmEyRg9k)
                                  padding: EdgeInsets.fromLTRB(
                                      7 * fem, 3 * fem, 6 * fem, 3 * fem),
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Color(0xffd9d9d9),
                                    borderRadius:
                                        BorderRadius.circular(5 * fem),
                                  ),
                                  child: Align(
                                    // ideatQW (607:1215)
                                    alignment: Alignment.topCenter,
                                    child: SizedBox(
                                      width: 60 * fem,
                                      height: 60 * fem,
                                      child: TextButton(
                                        onPressed: () {},
                                        style: TextButton.styleFrom(
                                          padding: EdgeInsets.zero,
                                        ),
                                        child: Image.asset(
                                          'assets/page-1/images/idea.png',
                                          fit: BoxFit.contain,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            // autogroup2q5kQdk (jiwfxq7DfX2m6deZK2Q5k)
                            margin: EdgeInsets.fromLTRB(
                                11 * fem, 0 * fem, 11 * fem, 44 * fem),
                            width: double.infinity,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  // keluhkanM3C (607:1216)
                                  margin: EdgeInsets.fromLTRB(
                                      0 * fem, 0 * fem, 68 * fem, 0 * fem),
                                  child: Text(
                                    'Keluhkan',
                                    style: SafeGoogleFont(
                                      'Buenard',
                                      fontSize: 12 * ffem,
                                      fontWeight: FontWeight.w700,
                                      height: 1.3 * ffem / fem,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ),
                                Container(
                                  // lokasianda3Rp (607:1217)
                                  margin: EdgeInsets.fromLTRB(
                                      0 * fem, 1 * fem, 78 * fem, 0 * fem),
                                  child: Text(
                                    'Lokasi Anda',
                                    style: SafeGoogleFont(
                                      'Buenard',
                                      fontSize: 12 * ffem,
                                      fontWeight: FontWeight.w700,
                                      height: 1.3 * ffem / fem,
                                      color: Color(0xff000000),
                                    ),
                                  ),
                                ),
                                Text(
                                  // edukasikr2 (607:1218)
                                  'Edukasi',
                                  style: SafeGoogleFont(
                                    'Buenard',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w700,
                                    height: 1.3 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            // autogroupwkiwhWN (jiwo3TeLfp4CByt2ZWKiW)
                            width: double.infinity,
                            height: 73 * fem,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  // autogrouphtzpEWJ (jiwu87r4AitWKzCRHHTZp)
                                  padding: EdgeInsets.fromLTRB(
                                      13 * fem, 16 * fem, 10 * fem, 7 * fem),
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Color(0xffd9d9d9),
                                    borderRadius:
                                        BorderRadius.circular(5 * fem),
                                  ),
                                  child: Center(
                                    // image139tA (607:1230)
                                    child: SizedBox(
                                      width: 50 * fem,
                                      height: 50 * fem,
                                      child: TextButton(
                                        onPressed: () {},
                                        style: TextButton.styleFrom(
                                          padding: EdgeInsets.zero,
                                        ),
                                        child: Image.asset(
                                          'assets/page-1/images/image-13.png',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 52 * fem,
                                ),
                                Container(
                                  // autogroup52n2FAW (jiwxNXS84iWeRrRRW52N2)
                                  padding: EdgeInsets.fromLTRB(
                                      12 * fem, 16 * fem, 11 * fem, 7 * fem),
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Color(0xffd9d9d9),
                                    borderRadius:
                                        BorderRadius.circular(5 * fem),
                                  ),
                                  child: Center(
                                    // image14y6W (607:1231)
                                    child: SizedBox(
                                      width: 50 * fem,
                                      height: 50 * fem,
                                      child: TextButton(
                                        onPressed: () {},
                                        style: TextButton.styleFrom(
                                          padding: EdgeInsets.zero,
                                        ),
                                        child: Image.asset(
                                          'assets/page-1/images/image-14-odg.png',
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 52 * fem,
                                ),
                                Container(
                                  // autogroupqzjrsBt (jix1hmDURKpNwfUMGQzJr)
                                  padding: EdgeInsets.fromLTRB(
                                      12 * fem, 12 * fem, 11 * fem, 11 * fem),
                                  height: double.infinity,
                                  decoration: BoxDecoration(
                                    color: Color(0xffd9d9d9),
                                    borderRadius:
                                        BorderRadius.circular(5 * fem),
                                  ),
                                  child: Center(
                                    // image16o5Y (607:1232)
                                    child: SizedBox(
                                      width: 50 * fem,
                                      height: 50 * fem,
                                      child: Image.asset(
                                        'assets/page-1/images/image-16.png',
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    // bellKpa (607:1210)
                    left: 347 * fem,
                    top: 111 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 50 * fem,
                        child: TextButton(
                          onPressed: () {},
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Image.asset(
                            'assets/page-1/images/bell.png',
                            width: 50 * fem,
                            height: 50 * fem,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // ambulance1pmL (607:1228)
                    left: 245 * fem,
                    top: 17 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 116 * fem,
                        height: 79 * fem,
                        child: Image.asset(
                          'assets/page-1/images/ambulance-1-uRQ.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // searchbar9Yi (607:1233)
                    left: 25 * fem,
                    top: 368 * fem,
                    child: Container(
                      width: 355 * fem,
                      height: 46 * fem,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Color(0x3f000000),
                            offset: Offset(0 * fem, 4 * fem),
                            blurRadius: 2 * fem,
                          ),
                        ],
                      ),
                      child: Container(
                        // content4fg (I607:1233;2:44)
                        padding: EdgeInsets.fromLTRB(
                            8 * fem, 6 * fem, 8 * fem, 1 * fem),
                        width: double.infinity,
                        height: 36 * fem,
                        decoration: BoxDecoration(
                          color: Color(0xedfafafa),
                          borderRadius: BorderRadius.circular(10 * fem),
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Opacity(
                              // imicandroidWGn (I607:1233;2:45)
                              opacity: 0,
                              child: Container(
                                margin: EdgeInsets.fromLTRB(
                                    0 * fem, 0 * fem, 118.5 * fem, 5 * fem),
                                width: 24 * fem,
                                height: 24 * fem,
                                child: Image.asset(
                                  'assets/page-1/images/i-mic-android-bP4.png',
                                  width: 24 * fem,
                                  height: 24 * fem,
                                ),
                              ),
                            ),
                            Container(
                              // contzSr (I607:1233;2:46)
                              margin: EdgeInsets.fromLTRB(
                                  0 * fem, 1 * fem, 125.5 * fem, 0 * fem),
                              height: 28 * fem,
                              child: Text(
                                'Search',
                                style: SafeGoogleFont(
                                  'SF Pro Text',
                                  fontSize: 17 * ffem,
                                  fontWeight: FontWeight.w400,
                                  height: 1.2941176471 * ffem / fem,
                                  letterSpacing: -0.4079999924 * fem,
                                  color: Color(0x993c3c43),
                                ),
                              ),
                            ),
                            Container(
                              // micHRx (I607:1233;2:49)
                              margin: EdgeInsets.fromLTRB(
                                  0 * fem, 0 * fem, 0 * fem, 5 * fem),
                              width: 24 * fem,
                              height: 24 * fem,
                              child: Image.asset(
                                'assets/page-1/images/mic-utr.png',
                                width: 24 * fem,
                                height: 24 * fem,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // Pzn (607:1234)
                    left: 66 * fem,
                    top: 120 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 274 * fem,
                        height: 224 * fem,
                        child: Image.asset(
                          'assets/gif/mental.gif',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle13XLJ (607:1265)
                    left: 98 * fem,
                    top: 339 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 208 * fem,
                        height: 118 * fem,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10 * fem),
                            border: Border.all(color: Color(0xffc4c4c4)),
                            color: Color(0xffebebeb),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                offset: Offset(0 * fem, 4 * fem),
                                blurRadius: 2 * fem,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // andayakinooc (607:1266)
                    left: 135.431640625 * fem,
                    top: 351.296295166 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 100 * fem,
                        height: 33 * fem,
                        child: Text(
                          'Anda Yakin',
                          style: SafeGoogleFont(
                            'Catamaran',
                            fontSize: 20 * ffem,
                            fontWeight: FontWeight.w700,
                            height: 1.64 * ffem / fem,
                            color: Color(0xff000000),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // rectangle41WT8 (618:975)
                    left: 178 * fem,
                    top: 392 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 50 * fem,
                        height: 29 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(9 * fem),
                              color: Color(0xffff006b),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // yapTp (618:973)
                    left: 187 * fem,
                    top: 389 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 31 * fem,
                        height: 40 * fem,
                        child: TextButton(
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginPage()),
                            );
                          },
                          style: TextButton.styleFrom(
                            padding: EdgeInsets.zero,
                          ),
                          child: Text(
                            'YA',
                            style: SafeGoogleFont(
                              'Catamaran',
                              fontSize: 24 * ffem,
                              fontWeight: FontWeight.w700,
                              height: 1.64 * ffem / fem,
                              color: Color(0xff000000),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    // cancel17C2 (610:972)
                    left: 251 * fem,
                    top: 346 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 36 * fem,
                        height: 36 * fem,
                        child: Image.asset(
                          'assets/page-1/images/cancel-1.png',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // autogroupmreaRiW (jixZc1jAAxGd4hvvfMrEa)
              margin:
                  EdgeInsets.fromLTRB(52 * fem, 0 * fem, 52 * fem, 35 * fem),
              width: double.infinity,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // presentasimGa (607:1219)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 66 * fem, 0 * fem),
                    child: Text(
                      'Presentasi',
                      style: SafeGoogleFont(
                        'Buenard',
                        fontSize: 12 * ffem,
                        fontWeight: FontWeight.w700,
                        height: 1.3 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                  Container(
                    // cekfasilitasgeS (607:1220)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 75 * fem, 0 * fem),
                    child: Text(
                      'Cek Fasilitas',
                      style: SafeGoogleFont(
                        'Buenard',
                        fontSize: 12 * ffem,
                        fontWeight: FontWeight.w700,
                        height: 1.3 * ffem / fem,
                        color: Color(0xff000000),
                      ),
                    ),
                  ),
                  Text(
                    // keluar1Av (607:1221)
                    'Keluar',
                    style: SafeGoogleFont(
                      'Buenard',
                      fontSize: 12 * ffem,
                      fontWeight: FontWeight.w700,
                      height: 1.3 * ffem / fem,
                      color: Color(0xff000000),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // group119Y2 (607:1223)
              margin: EdgeInsets.fromLTRB(8 * fem, 0 * fem, 0 * fem, 56 * fem),
              width: 385 * fem,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // autogrouperd8HPL (jiyazUSVMbzfRh49ceRD8)
                    margin: EdgeInsets.fromLTRB(
                        0 * fem, 0 * fem, 0 * fem, 23 * fem),
                    padding: EdgeInsets.fromLTRB(
                        15 * fem, 14 * fem, 17 * fem, 18 * fem),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Color(0xffe7e7e7),
                      borderRadius: BorderRadius.circular(5 * fem),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x3f000000),
                          offset: Offset(0 * fem, 4 * fem),
                          blurRadius: 2 * fem,
                        ),
                      ],
                    ),
                    child: Center(
                      // birudanhijauillustrasiselamath (607:1227)
                      child: SizedBox(
                        width: 353 * fem,
                        height: 562 * fem,
                        child: Image.asset(
                          'assets/gif/1.gif',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    // autogroup7btcttv (jiyeVNcQdSfamPkvU7BTC)
                    padding: EdgeInsets.fromLTRB(
                        16 * fem, 23 * fem, 15 * fem, 25 * fem),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Color(0xffe7e7e7),
                      borderRadius: BorderRadius.circular(5 * fem),
                      boxShadow: [
                        BoxShadow(
                          color: Color(0x3f000000),
                          offset: Offset(0 * fem, 4 * fem),
                          blurRadius: 2 * fem,
                        ),
                      ],
                    ),
                    child: Center(
                      // kremdanorenilustrasicaracepatd (607:1226)
                      child: SizedBox(
                        width: 354 * fem,
                        height: 396 * fem,
                        child: Image.asset(
                          'assets/gif/2.gif',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              // ketentuanpenggunaankebijakanpr (607:1209)
              margin: EdgeInsets.fromLTRB(9 * fem, 0 * fem, 0 * fem, 0 * fem),
              constraints: BoxConstraints(
                maxWidth: 230 * fem,
              ),
              child: Text(
                'Ketentuan Penggunaan | Kebijakan Privasi\nUnduh Buku Panduan',
                textAlign: TextAlign.center,
                style: SafeGoogleFont(
                  'Gilda Display',
                  fontSize: 12 * ffem,
                  fontWeight: FontWeight.w400,
                  height: 1.1775 * ffem / fem,
                  color: Color(0xff000000),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
