import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/gestures.dart';
import 'dart:ui';
import 'package:google_fonts/google_fonts.dart';
import 'package:login_flutter/first.dart';
import 'package:login_flutter/login.dart';
import 'package:login_flutter/utils.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key});

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
  }

  @override
  Widget build(BuildContext context) {
    double baseWidth = 430;
    double fem = MediaQuery.of(context).size.width / baseWidth;
    double ffem = fem * 0.97;
    return Container(
      width: double.infinity,
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Color(0xffffffff),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: double.infinity,
              height: 37 * fem,
              decoration: BoxDecoration(
                color: Color(0xff5146a6),
                borderRadius: BorderRadius.only(
                  bottomRight: Radius.circular(7 * fem),
                  bottomLeft: Radius.circular(7 * fem),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              height: 790 * fem,
              child: Stack(
                children: [
                  Positioned(
                    left: 153 * fem,
                    top: -22 * fem,
                    child: Container(
                      width: 122 * fem,
                      height: 117 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 122 * fem,
                                height: 117 * fem,
                                child: Image.asset(
                                  'assets/images/group.png',
                                  width: 122 * fem,
                                  height: 117 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 6 * fem,
                            top: 74 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 114 * fem,
                                height: 11 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Color(0xffffffff),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 37 * fem,
                            top: 74 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 53 * fem,
                                height: 24 * fem,
                                child: RichText(
                                  text: TextSpan(
                                    style: SafeGoogleFont(
                                      'Miltonian Tattoo',
                                      fontSize: 20 * ffem,
                                      fontWeight: FontWeight.w400,
                                      height: 1.1920000076 * ffem / fem,
                                      color: Color(0xff000000),
                                    ),
                                    children: [
                                      TextSpan(
                                        text: 'ja',
                                        style: SafeGoogleFont(
                                          'Miltonian Tattoo',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w400,
                                          height: 1.1925 * ffem / fem,
                                          color: Color(0xffff42ec),
                                        ),
                                      ),
                                      TextSpan(
                                        text: 'b',
                                        style: SafeGoogleFont(
                                          'Miltonian Tattoo',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w400,
                                          height: 1.1925 * ffem / fem,
                                          color: Color(0xff5146a6),
                                        ),
                                      ),
                                      TextSpan(
                                        text: 'ar',
                                        style: SafeGoogleFont(
                                          'Miltonian Tattoo',
                                          fontSize: 20 * ffem,
                                          fontWeight: FontWeight.w400,
                                          height: 1.1925 * ffem / fem,
                                          color: Color(0xffff42ec),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 89 * fem,
                    child: Container(
                      width: 1229 * fem,
                      height: 199 * fem,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7 * fem),
                      ),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                0 * fem, 0 * fem, 0 * fem, 2 * fem),
                            width: 399 * fem,
                            height: 197 * fem,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(7 * fem),
                              child: Image.asset(
                                'assets/images/rectangle-46.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16 * fem,
                          ),
                          Container(
                            width: 399 * fem,
                            height: 197 * fem,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(7 * fem),
                              child: Image.asset(
                                'assets/images/rectangle-1.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 16 * fem,
                          ),
                          Container(
                            width: 399 * fem,
                            height: 197 * fem,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(7 * fem),
                              child: Image.asset(
                                'assets/images/rectangle-45.png',
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 355 * fem,
                    child: Align(
                      child: SizedBox(
                        width: 399 * fem,
                        height: 27 * fem,
                        child: Image.asset(
                          'assets/images/auto-group-wlr1.png',
                          width: 399 * fem,
                          height: 27 * fem,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 300 * fem,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(
                          8 * fem, 14 * fem, 8 * fem, 14 * fem),
                      width: 399 * fem,
                      height: 45 * fem,
                      decoration: BoxDecoration(
                        color: Color(0x47343434),
                        borderRadius: BorderRadius.circular(7 * fem),
                      ),
                      child: Text(
                        'Museum di Jawa Barat',
                        style: SafeGoogleFont(
                          'Roboto Flex',
                          fontSize: 14 * ffem,
                          fontWeight: FontWeight.w500,
                          height: 1.1725 * ffem / fem,
                          color: Color(0xff000000),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 396 * fem,
                    child: Container(
                      width: 399 * fem,
                      height: 37 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 2 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 399 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(6 * fem),
                                      topRight: Radius.circular(7 * fem),
                                      bottomRight: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xff5146a6),
                                        offset: Offset(2 * fem, 2 * fem),
                                        blurRadius: 0 * fem,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 35 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xff5146a6),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 8 * fem,
                            top: 7 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 19.71 * fem,
                                height: 20.63 * fem,
                                child: Image.asset(
                                  'assets/images/vector-hCz.png',
                                  width: 19.71 * fem,
                                  height: 20.63 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 381 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 11 * fem,
                                child: Image.asset(
                                  'assets/images/vector-cAi.png',
                                  width: 7 * fem,
                                  height: 11 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 42 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 62 * fem,
                                height: 15 * fem,
                                child: Text(
                                  'Kota Depok',
                                  style: SafeGoogleFont(
                                    'Roboto Flex',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.1725 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 437 * fem,
                    child: Container(
                      width: 399 * fem,
                      height: 37 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 2 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 399 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(6 * fem),
                                      topRight: Radius.circular(7 * fem),
                                      bottomRight: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xff5146a6),
                                        offset: Offset(2 * fem, 2 * fem),
                                        blurRadius: 0 * fem,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 35 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xff5146a6),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 42 * fem,
                            top: 11 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 37 * fem,
                                height: 15 * fem,
                                child: Text(
                                  'Ciamis',
                                  style: SafeGoogleFont(
                                    'Roboto Flex',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.1725 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 8 * fem,
                            top: 7 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 19.71 * fem,
                                height: 20.63 * fem,
                                child: Image.asset(
                                  'assets/images/vector-1M8.png',
                                  width: 19.71 * fem,
                                  height: 20.63 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 381 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 11 * fem,
                                child: Image.asset(
                                  'assets/images/vector.png',
                                  width: 7 * fem,
                                  height: 11 * fem,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 480 * fem,
                    child: Container(
                      width: 399 * fem,
                      height: 37 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 2 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 399 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(6 * fem),
                                      topRight: Radius.circular(7 * fem),
                                      bottomRight: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xff5146a6),
                                        offset: Offset(2 * fem, 2 * fem),
                                        blurRadius: 0 * fem,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 35 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xff5146a6),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 42 * fem,
                            top: 11 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 54 * fem,
                                height: 15 * fem,
                                child: Text(
                                  'Karawang',
                                  style: SafeGoogleFont(
                                    'Roboto Flex',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.1725 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 8 * fem,
                            top: 7 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 19.71 * fem,
                                height: 20.63 * fem,
                                child: Image.asset(
                                  'assets/images/vector-EYn.png',
                                  width: 19.71 * fem,
                                  height: 20.63 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 381 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 11 * fem,
                                child: Image.asset(
                                  'assets/images/vector-P2E.png',
                                  width: 7 * fem,
                                  height: 11 * fem,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 523 * fem,
                    child: Container(
                      width: 399 * fem,
                      height: 37 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 2 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 399 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(6 * fem),
                                      topRight: Radius.circular(7 * fem),
                                      bottomRight: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xff5146a6),
                                        offset: Offset(2 * fem, 2 * fem),
                                        blurRadius: 0 * fem,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 35 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xff5146a6),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 42 * fem,
                            top: 11 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 75 * fem,
                                height: 15 * fem,
                                child: Text(
                                  'Kota Bandung',
                                  style: SafeGoogleFont(
                                    'Roboto Flex',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.1725 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 8 * fem,
                            top: 7 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 19.71 * fem,
                                height: 20.63 * fem,
                                child: Image.asset(
                                  'assets/images/vector-dW6.png',
                                  width: 19.71 * fem,
                                  height: 20.63 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 381 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 11 * fem,
                                child: Image.asset(
                                  'assets/images/vector-uQz.png',
                                  width: 7 * fem,
                                  height: 11 * fem,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 566 * fem,
                    child: Container(
                      width: 399 * fem,
                      height: 37 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 2 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 399 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(6 * fem),
                                      topRight: Radius.circular(7 * fem),
                                      bottomRight: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xff5146a6),
                                        offset: Offset(2 * fem, 2 * fem),
                                        blurRadius: 0 * fem,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 42 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 79 * fem,
                                height: 15 * fem,
                                child: Text(
                                  'Bandung Barat\n',
                                  style: SafeGoogleFont(
                                    'Roboto Flex',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.1725 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 35 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xff5146a6),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 8 * fem,
                            top: 7 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 19.71 * fem,
                                height: 20.63 * fem,
                                child: Image.asset(
                                  'assets/images/vector-2CW.png',
                                  width: 19.71 * fem,
                                  height: 20.63 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 381 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 11 * fem,
                                child: Image.asset(
                                  'assets/images/vector-7Uz.png',
                                  width: 7 * fem,
                                  height: 11 * fem,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 609 * fem,
                    child: Container(
                      width: 399 * fem,
                      height: 37 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 2 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 399 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(6 * fem),
                                      topRight: Radius.circular(7 * fem),
                                      bottomRight: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xff5146a6),
                                        offset: Offset(2 * fem, 2 * fem),
                                        blurRadius: 0 * fem,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 35 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xff5146a6),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 42 * fem,
                            top: 11 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 60 * fem,
                                height: 15 * fem,
                                child: Text(
                                  'Kota Bogor',
                                  style: SafeGoogleFont(
                                    'Roboto Flex',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.1725 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 8 * fem,
                            top: 7 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 19.71 * fem,
                                height: 20.63 * fem,
                                child: Image.asset(
                                  'assets/images/vector-eEi.png',
                                  width: 19.71 * fem,
                                  height: 20.63 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 381 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 11 * fem,
                                child: Image.asset(
                                  'assets/images/vector-ZAN.png',
                                  width: 7 * fem,
                                  height: 11 * fem,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 652 * fem,
                    child: Container(
                      width: 399 * fem,
                      height: 37 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 2 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 399 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(6 * fem),
                                      topRight: Radius.circular(7 * fem),
                                      bottomRight: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xff5146a6),
                                        offset: Offset(2 * fem, 2 * fem),
                                        blurRadius: 0 * fem,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 35 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xff5146a6),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 42 * fem,
                            top: 11 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 42 * fem,
                                height: 15 * fem,
                                child: Text(
                                  'Cirebon',
                                  style: SafeGoogleFont(
                                    'Roboto Flex',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.1725 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 8 * fem,
                            top: 7 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 19.71 * fem,
                                height: 20.63 * fem,
                                child: Image.asset(
                                  'assets/images/vector-RZY.png',
                                  width: 19.71 * fem,
                                  height: 20.63 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 381 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 11 * fem,
                                child: Image.asset(
                                  'assets/images/vector-YjQ.png',
                                  width: 7 * fem,
                                  height: 11 * fem,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Positioned(
                    left: 15 * fem,
                    top: 695 * fem,
                    child: Container(
                      width: 399 * fem,
                      height: 37 * fem,
                      child: Stack(
                        children: [
                          Positioned(
                            left: 0 * fem,
                            top: 2 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 399 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xffffffff),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(6 * fem),
                                      topRight: Radius.circular(7 * fem),
                                      bottomRight: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color(0xff5146a6),
                                        offset: Offset(2 * fem, 2 * fem),
                                        blurRadius: 0 * fem,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 0 * fem,
                            top: 0 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 35 * fem,
                                height: 35 * fem,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: Color(0xffffffff)),
                                    color: Color(0xff5146a6),
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(7 * fem),
                                      bottomLeft: Radius.circular(7 * fem),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 42 * fem,
                            top: 11 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 81 * fem,
                                height: 15 * fem,
                                child: Text(
                                  'Kota Sukabumi',
                                  style: SafeGoogleFont(
                                    'Roboto Flex',
                                    fontSize: 12 * ffem,
                                    fontWeight: FontWeight.w600,
                                    height: 1.1725 * ffem / fem,
                                    color: Color(0xff000000),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 8 * fem,
                            top: 7 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 19.71 * fem,
                                height: 20.63 * fem,
                                child: Image.asset(
                                  'assets/images/vector-mqg.png',
                                  width: 19.71 * fem,
                                  height: 20.63 * fem,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                            left: 381 * fem,
                            top: 13 * fem,
                            child: Align(
                              child: SizedBox(
                                width: 7 * fem,
                                height: 11 * fem,
                                child: Image.asset(
                                  'assets/images/vector-YjQ.png',
                                  width: 7 * fem,
                                  height: 11 * fem,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      signOut().then((value) {
                        // Navigasi ke halaman login setelah logout berhasil
                        Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(builder: (context) => loginpage()),
                        );
                      });
                    },
                    child: Text("Logout"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
